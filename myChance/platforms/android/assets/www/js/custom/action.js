function getBaseUrl(){
	var baseUrl = "http://m.mychance.in/";
	return baseUrl;
}
function getLocalStorageItem(key){
	if(window.localStorage.getItem(key)!=null)
		return window.localStorage.getItem(key);
}

$(document).on('submit', '#ossn-wall-form', function(e) {
    e.preventDefault();
	var data = {
		"guid": getLocalStorageItem("userid"),
		"post": $("#post").val(),
		"friends": $("#ossn-wall-friend-input").val(),
		"location": $("#ossn-wall-location-input").val(),
		"privacy":""
	}
	$.ajax({
		url: getBaseUrl()+"restCalls/application/wall/post/home.php",
		type: "POST",
		crossDomain:true,
		data: data,
		beforeSend: function(){
			$('#wall-post-submit').hide();
			$('.ossn-loading').removeClass("ossn-hidden");
		},
		success: function(data){
			console.log(data);
			data = $.parseJSON(data);
			if(!data.hasOwnProperty("errmessage")){
				//alert(console.log(data.template));
				$('.user-activity').prepend(data.template);
			}
			else
				alert(data.errmessage);
		},
		error: function(xhr, status, error){
			alert(xhr.responseText);
		},
		complete: function(){
			$('.ossn-loading').addClass("ossn-hidden");
			$('#wall-post-submit').show();
		}
	});
});

