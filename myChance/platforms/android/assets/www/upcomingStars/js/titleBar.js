/**
 *	Function to check messenger appplication available or not in device
 *	if available -> show 'showMessenger' button else -> show 'downloadMessenger' button
 */
function checkMessenger() {
    //document.getElementById("contentIframe").contentDocument.location.reload(true);
    appAvailability.check(
        'com.mychance.chat', // Package Name
        function() { // Success callback
            $(".showMessenger").show();
            $(".downloadMessenger").hide();
        },
        function() { // Error callback
            $(".downloadMessenger").show();
            $(".showMessenger").hide();
        }
    );
}


/*
	Function to check network connection on every second
*/
setInterval(onlineOffline, 1000);

/*
	Function to check internet connection
*/
function onlineOffline() {
    var url = document.getElementById("contentIframe").src;
    if (navigator.connection.type == 'none') {
        login = false;
        // if offline
        if (url.indexOf("error.html") <= 0) {
            // if not showing error page
            $('#contentIframe').attr('src', "error.html");
            $(".myTitleBar").css("display", "none");
        }
        return false;
    } else {
        // if online
        if (url.indexOf("error.html") > 0) {
            // if currently displaying erro page; login me in
            var userName = storage.getItem("userName");
            var passWord = storage.getItem("passWord");
            var oldToken = storage.getItem("oldToken");
            var newToken = storage.getItem("newToken");
            if (userName.length > 0 && passWord.length > 0)
                loginServer(userName, passWord, oldToken, newToken);
            else {
                $("#loginPage").css("display", "block");
            }
        }
        return true;
    }
}

/*
	function to hide message after 3 seconds
*/
setTimeout(function() {
    $("#errorMsg").hide(500);
}, 3000);

/**
	Function to call notification and request check method on every 5 seconds
*/
setInterval(function() {
    if (login)
        checkNotificationsAndRequests();
}, 5000);

app.initialize();

/*
	Function invoked when login button clicks
*/
$("#login").click(function(e) {
    fresh = false;
    var userName = $("#userName").val();
    var passWord = $("#password").val();
    var oldToken = storage.getItem("oldToken");
    var newToken = deviceId;
    if (userName != '' && passWord != '') {
        loginServer(userName, passWord, oldToken, newToken);
    } else {
        if (userName == '')
            $("#userName").focus();
        else
            $("#password").focus();
    }
});
var id;

/**
	Login user with server
*/
function loginServer(userName, passWord, oldToken, newToken) {
    $.ajax({
        type: "GET",
        crossDomain: true,
        url: url + "/restCalls/firstLogin.php",
        data: {
            "userName": userName,
            "password": passWord,
            "oldToken": oldToken,
            "newToken": newToken
        },
        success: function(data) {
            var obj = $.parseJSON(data);
            if (!$.isEmptyObject(obj)) {
                var name = obj.fullName;
                id = obj.id;
                $("#myName").text(name);
                $("#userProPic").attr("src", url + "/avatar/" + userName + "/small");
                $(".myTitleBar").css("display", "block");
                $("#loginPage").css("display", "none");
                $('#contentIframe').attr('src', url + "/restCalls/login.php?userName=" + userName + "&password=" + passWord + "&newToken=" + newToken);
                $("#contentIframe").css("display", "block");
                login = true;
                // store credentials to local storage
                storage.setItem("userName", userName);
                storage.setItem("usertype", obj.type);				
                storage.setItem("userid", id);				
                storage.setItem("passWord", passWord);
                storage.setItem("oldToken", deviceId);
                storage.setItem("newToken", "");
            } else {
                console.log("login failed");
                $("#errorMsg").text("Invalid Login");
                $("#errorMsg").css("display", "block");
                $("#loginPage").css("display", "block");
            }


        },
        error: function() {
            $("#errorMsg").text("Invalid Login");
            $("#errorMsg").css("display", "block");
        }
    });

}

/**
 *	function to check if the user session is active or not
 */
function checkSession() {
    onlineOffline();
    if (login) {
        var userName = storage.getItem("userName");
        var newToken = storage.getItem("oldToken");
        checkMySession(userName, newToken);
    }
}

/*
	Function to check user's session
*/
function checkMySession(userName, newToken) {
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/checkLogin.php",
        data: {
            "userName": userName,
            "deviceId": newToken
        },
        success: function(data) {
            if (data == "1") {
                // checkNotificationsAndRequests();
                login = true;
            } else {
                $("#contentIframe").css("display", "none");
                $('#contentIframe').attr('src', "");
                $("#loginPage").css("display", "block");
                if (login) {
                    $("#errorMsg").text("Logout Successful.");
                    $("#errorMsg").css("display", "block");
                }
                // remove un,pwd & token from localStorage
                storage.removeItem("userName");
                storage.removeItem("passWord");
                storage.removeItem("newToken");
                storage.removeItem("oldToken");
				storage.removeItem("usertype");
				storage.removeItem("userid");

                login = false;
            }
        },
        error: function() {}
    });
}

/*
	Function to check new unread notifications, messages & requests
*/
function checkNotificationsAndRequests() {
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/notifications.php",
        data: {
            "id": id
        },
        success: function(data) {
            var obj = $.parseJSON(data);
            console.log(obj);
            if (parseInt(obj.friends) > 0) {
                $("#frndRqstBdge").text(obj.friends);
                $("#frndRqstBdge").show();
            } else

                $("#frndRqstBdge").hide();
            if (parseInt(obj.notification) > 0) {
                $("#ntfctnBdg").text(obj.notification);
                $("#ntfctnBdg").show();
            } else
                $("#ntfctnBdg").hide();
            if (parseInt(obj.message) > 0) {
                $("#messageBdg").text(obj.message);
                $("#messageBdg").show();
            } else
                $("#messageBdg").hide();
        },
        error: function() {
            alert();
        }
    });
}

/*
	Function to hide message after clicking on any of the text box
*/
$(".elmnt").click(function() {
    $("#errorMsg").hide(500)
});

/*
	Function to launch messenger applciation
*/
function showMessenger() {
    var sApp = startApp.set({
        "action": "ACTION_MAIN",
        "category": "CATEGORY_DEFAULT",
        "type": "text/css",
        "package": "com.mychance.chat",
        "uri": "file://data/index.html",
        "flags": ["FLAG_ACTIVITY_CLEAR_TOP", "FLAG_ACTIVITY_CLEAR_TASK"],
        "intentstart": "startActivity"
    });
    sApp.start(function() {}, function(error) {});
}

/*
	Function to show registration page
*/
function showRegistrationPage() {
    $("#loginPage").hide();
    $("#registerPage").show();
}

/*
	Function to show login page
*/
function showLoginPage() {
    $("#loginPage").show();
    $("#registerPage").hide();
}

/*
	Function to register user, invoked on register button click
*/
function registerUser() {
    var firstname = $("#firstName").val();
    var lastName = $("#lastName").val();
    var email = $("#email").val();
    var confirmEmail = $("#confirmEmail").val();
    var userNameRgstr = $("#rgstrUsername").val();
    var passwordRgstr = $("#rgstrPassword").val();
    var dob = $("#birthDate").val();
    var gender = $("#gender").val();
    if (confirmEmail != email) {
        alert("Emails must match");
    } else {
        $.ajax({
            type: "POST",
            crossDomain: true,
            url: url + "/restCalls/register.php",
            data: {
                "firstname": firstname,
                "lastname": lastName,
                "email": email,
                "password": passwordRgstr,
                "username": userNameRgstr,
                "dob": dob,
                "gender": gender
            },
            success: function(data) {
                if (data.success == "1") {
                    showLoginPage();
                    $("#errorMsg").text(data.message);
                    $("#errorMsg").css("display", "block");
                } else {
                    $("#rgstrMessage").text(data.message);
                    $(".myMessage").fadeIn(500);
                    setTimeout(function() {
                       // $(".myMessage").fadeOut(500);
                    }, 3000);
                }
            },
            error: function() {
                console.log("Registration Failed");
            }
        });
    }
}

/*
	Show and hide side bar
*/
$("#sidebar-toggle").click(function() {
    if ($("#sidebar-toggle").hasClass("shower")) {
        $("#sidebar-toggle").removeClass("shower");
        $("#sidebar-toggle").addClass("hider");
        $(".sidebar").addClass("sidebar-open");
        $(".ossn-page-container").addClass("sidebar-open-page-container");
    } else {
        $("#sidebar-toggle").removeClass("hider");
        $("#sidebar-toggle").addClass("shower");
        $(".sidebar").removeClass("sidebar-open");
        $(".sidebar").addClass("sidebar-close");
        $(".ossn-page-container").removeClass("sidebar-open-page-container");
    }
});

/*
	Show/hide menu list
*/
$("#menuLister").click(function() {
    $("#menuList").toggle(500);
});

/*
	show/hide group list
*/
$("#groupLister").click(function() {
    $("#groupList").toggle(500);
});

/*
	Function to load new url to iframe
*/
function loadMe(myUrl) {
    var online = onlineOffline();
    if (online) {
        if (myUrl.indexOf('logout.php') > 0) {
            logoutMe();
        } else {
            var newUrl = myUrl.replace("userName", storage.getItem("userName"));
            $('#contentIframe').attr('src', newUrl);
            $("#sidebar-toggle").removeClass("hider");
            $("#sidebar-toggle").addClass("shower");
            $(".sidebar").removeClass("sidebar-open");
            $(".sidebar").addClass("sidebar-close");
            $(".ossn-page-container").removeClass("sidebar-open-page-container");
        }
    }
}

/*
	Function to logout user
*/
function logoutMe() {
    var userName = storage.getItem("userName");
    var newToken = storage.getItem("oldToken");
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/logout.php",
        data: {
            "userName": userName,
            "deviceId": newToken
        },
        success: function(data) {
            console.log(data);
            if (data == "1") {
                console.log("Logout success");
                $("#contentIframe").css("display", "none");
                $('#contentIframe').attr('src', "");
                $("#loginPage").css("display", "block");
                if (login) {
                    $("#errorMsg").text("Logout Successful.");
                    $("#errorMsg").css("display", "block");
                }
                // remove un,pwd & token from localStorage
                storage.setItem("userName", "");
                storage.setItem("passWord", "");
                storage.setItem("newToken", "");
                storage.setItem("oldToken", "");

                login = false;
            } else {
                console.log("Logout Failed");
            }
        },
        error: function() {
            console.log("Logout Failed");
        }
    });
}

/**/
$('#searchBox').on("keyup",function(e){
	if(e.which == 13) {
	searchMe($("#searchBox").val());
}
});


/*
	Function to search user
*/
function searchMe(val){
$('#contentIframe').attr('src', url+"/search?q="+val);
$("#searchBox").val('');
$("#sidebar-toggle").removeClass("hider");
            $("#sidebar-toggle").addClass("shower");
            $(".sidebar").removeClass("sidebar-open");
            $(".sidebar").addClass("sidebar-close");
            $(".ossn-page-container").removeClass("sidebar-open-page-container");
}

/*
	Function to toggle notification list
*/
$("#notificationIcon").click(function() {
	// hide freind request list if displaying
	if ($("#friendRquestIcon").hasClass("hider")) {
        $("#friendRquestIcon").removeClass("hider");
        $("#friendRquestIcon").addClass("shower");
        $(".friendRequestDropDown").hide();
    }
	// show notifications
    if ($("#notificationIcon").hasClass("shower")) {
		$(".notificationDropDown").show();
        $("#notificationIcon").removeClass("shower");
        $("#notificationIcon").addClass("hider");
		getNotifcations();
    } else {
        $("#notificationIcon").removeClass("hider");
        $("#notificationIcon").addClass("shower");
        $(".notificationDropDown").hide();
    }
});



/*
	Function to fetch and list notifications
*/
function getNotifcations() {
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/getNotifications.php",
		data:{
			"userId":id
			},
        success: function(data) {
            $("#notificationArea").html(data);
			
        },
        error: function() {
            
        }
    });
}

/*
	Function to hide notification list
*/
function loadThisNotification(myUrl){
	var online = onlineOffline();
    if (online) {
        $('#contentIframe').attr('src', myUrl);
        $("#notificationIcon").removeClass("hider");
        $("#notificationIcon").addClass("shower");
        $(".notificationDropDown").hide();
    }
}

/*
	show post on notification click
*/
$(document).on('click', '.ossn-notifications-all a', function(event){
 event.preventDefault();
 loadThisNotification(this.href);
});
/*
	Function to mark all notifications as read
*/
function readAllNotification() {
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/markRead.php",
		data:{
			"userId":id
			},
        success: function(data) {
            console.log(data);
			$("#notificationIcon").removeClass("hider");
        $("#notificationIcon").addClass("shower");
        $(".notificationDropDown").hide();
        },
        error: function() {
            
        }
    });
}

/*
	Function to toggle friend reqeust list
*/
$("#friendRquestIcon").click(function() {
	// hide notification list of displaying
	if ($("#notificationIcon").hasClass("hider")) {
	    $("#notificationIcon").removeClass("hider");
        $("#notificationIcon").addClass("shower");
        $(".notificationDropDown").hide();
    }
	// show freind requests
    if ($("#friendRquestIcon").hasClass("shower")) {
		$(".friendRequestDropDown").show();
        $("#friendRquestIcon").removeClass("shower");
        $("#friendRquestIcon").addClass("hider");
		getFriendRequest();
    } else {
        $("#friendRquestIcon").removeClass("hider");
        $("#friendRquestIcon").addClass("shower");
        $(".friendRequestDropDown").hide();
    }
});

/*
	Function to get all requests
*/
function getFriendRequest(){
$.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/getFriendRequests.php",
		data:{
			"userId":id
			},
        success: function(data) {
            console.log(data);
			$("#requestArea").html(data);
        },
        error: function() {
            
        }
    });
}
/*
	Prevent default a action, show that url in iframe
*/
$(document).on('click', '.rqsts a', function(event){
 event.preventDefault();
 loadThisRequest(this.href);
});

/*
	Function to load requester profile
*/
function loadThisRequest(myUrl){
	var online = onlineOffline();
    if (online) {
        $('#contentIframe').attr('src', myUrl);
        $("#friendRquestIcon").removeClass("hider");
        $("#friendRquestIcon").addClass("shower");
        $(".friendRequestDropDown").hide();
    }
}

/*
	Function to confirm friend request
*/
function confirmFriend(friendId){
	$.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/addOrRemoveFriend.php",
		data:{
			"userId":id,
			"friendId":friendId,
			"action":"add"
			},
        success: function(data) {
            console.log(data);
			$("#friendRquestIcon").removeClass("hider");
        $("#friendRquestIcon").addClass("shower");
        $(".friendRequestDropDown").hide();
        },
        error: function() {
            
        }
    });
}

/*
	Function to remove friend request
*/
function removeFriend(friendId){
	$.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/addOrRemoveFriend.php",
		data:{
			"userId":id,
			"friendId":friendId,
			"action":"remove"
			},
        success: function(data) {
            console.log(data);
			$("#friendRquestIcon").removeClass("hider");
        $("#friendRquestIcon").addClass("shower");
        $(".friendRequestDropDown").hide();
        },
        error: function() {
            
        }
    });
}

function loadHtml(url){
	 $('#contentIframe').attr('src', url);
            $("#sidebar-toggle").removeClass("hider");
            $("#sidebar-toggle").addClass("shower");
            $(".sidebar").removeClass("sidebar-open");
            $(".sidebar").addClass("sidebar-close");
            $(".ossn-page-container").removeClass("sidebar-open-page-container");
}