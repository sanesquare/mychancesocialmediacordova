function getBaseUrl(){
	return "http://m.mychance.in";
}
function getAjaxUrl(){
	return getBaseUrl()+"/restCalls/application/mct/";
}
function saveAdAjaxUrl(){
	return "http://mychance.in/mtrade/xhr/xhr.php";
}

function getLocalStorageItem(key){
	return window.localStorage.getItem(key);
}

function removeLocalStorageItem(key){
	window.localStorage.removeItem(key);
}

$(document).on('ajaxStart', function() {
	$('.loader-wrapper').show();
	 $("body > *").not("body > .loader-wrapper").css('pointer-events', 'none');
	  $("body > *").not("body > .loader-wrapper").css('opacity', '0.5');
	/*$('body').not('.loader-wrapper').css('pointer-events', 'none');
	$('body').not('.loader-wrapper').css('opacity', '0.4');*/
});

$(document).on('ajaxComplete', function() {
	$('.loader-wrapper').hide();
	$('body').not('.loader-wrapper').css('pointer-events', 'auto');
	$('body').not('.loader-wrapper').css('opacity', '1');
});

function session_out(){
	removeLocalStorageItem('session_id');
}

function getSessionId(){
	var session_id = getLocalStorageItem("session_id");
	if(session_id=='' || session_id===null || session_id==undefined){
		var userid = getLocalStorageItem('userid');
		var username = getLocalStorageItem("username");
		var hash_session = $.md5(userid+username);
		window.localStorage.setItem("session_id", hash_session);
		console.log("session id generated: "+ hash_session);
		return $.md5(userid+username);
	}
	else
		return session_id;
}

function closeMenu(){
	if($('.menutrigger').hasClass('mtactive'))
		$('.menutrigger').click();
}

$(document).ready(function(e) {
   loadHome();
});

/*
	FUNCTION FOR LOADING HOME PAGE DATA
*/
function loadHome(){
	closeMenu();
	$.ajax({
		url: getAjaxUrl() + "home.php",
		type: "POST",
		crossDomain:true,
		data: {"guid": getLocalStorageItem("userid"),'session_id':getSessionId()},
		beforeSend: function(){
			
		},
		success: function(data){
			setHomeView(data);
		},
		error: function(xhr, status, error){
			console.log(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}
function setHomeView(data){
	var html = '<section id="categories-homepage" class="cat-box-contain" style="background-color:#ffffff;"> <div class="container"> <div class="section-header-style-1"> <div class="row"> <div class="col-xs-12"> <!--<h2 class="main-title" style="margin-top: 10px;">SERVICES</h2>--> <div class="main-sub-title"> </div> <!--<div class="h2-seprator"></div>--> </div> </div> </div><div class="row cat-wrapper" data-call="11">';
	var obj = $.parseJSON(data);
	if(obj.admin!=null && obj.admin!=undefined && !$('.admin-menu').length){
		$('#menu-main').append(obj.admin);
		$('.menu_table').css('width', '20%');
	}
	if(obj.status==400){
		alert(obj.message);
		return false;
	}
	html+='<section class="container m-t-60" style="margin-top:15px"> <div class="row"> <div class="col-md-9 col-sm-12 col-xs-12"> <section id="ads-homepage" class="category-page-ads clearfix"> <div class="row"> <div class="tab-content"> <div class="tab-pane active fade in latest-ads-holder" id="Tlatest"> <div class="latest-ads-grid-holder">';
		for(var i=0; obj.ads[i]!=null;i++){
			var ad = obj.ads[i];
			ad['image'] = ad['image']==null ? "images/noimg.jpg": ad['image'];
			html+='<div class="col-md-4 col-sm-6 col-xs-12" id="adv'+ad['ads_id']+'"> <div class="ad-box random-posts-grid " style="-moz-box-shadow: 0 0 5px #888;-webkit-box-shadow: 0 0 5px#888;box-shadow: 0 0 5px #888;height:200px;margin-bottom:15px"> <a class="ad-image subcategory-grid" data-id='+ad['ads_id']+' href="#" title=""> <img width="270" height="220" src="'+ad['image']+'" class="attachment-270x220 size-270x220 wp-post-image" sizes="(max-width: 270px) 100vw, 270px"> </a>';
			if(ad['price']!=null && ad['price']!=''){
				html+='<div class="add-price"><span> Qty. <!--<img src="images/rs.png" style="width: 8px;margin-top: -1px;">--> '+ad['price']+' </span> </div>';
		  	}
            html+='<div class="post-title-cat" style="background-color:rgba(0, 0, 0, 0.26)"> <div class="post-title subcategory-grid" data-id='+ad['ads_id']+' style="display: inline-block;"> <div class="post-title-icon" style="background-color:#53cc37"><i class="fa fa-book"></i></div> <a href="#" style="color:#fff;text-shadow:1px 1px #313131"> '+ad['title']+'</a> </div>';
			if(ad['posted_by']==obj['logged_in'] || obj.isAdmin ){
				html+='<div class="delete-ad-grid" style="display: inline-block;" onclick=confirmOp("deleteThisAd('+ad['ads_id']+')")> <i class="fa fa-trash"></i> </div>';
			}
		html+='</div> </div> </div>';
		}
		html+='</div> </div> </div> </div> </section> </div> </div> </section>'
	$('#content_area').html(html);
}
function getRandomColor() {
    var letters = '0123456789ABCDEF';
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
        color += letters[Math.floor(Math.random() * 16)];
    }
    return color;
}

/*
	FUNCTION TO LOAD SUBCATEGORY PAGE
*/
$(document).on('click', '.subcategory-name', function(event){
	event.preventDefault();
	var $this = $(this);
	var subcategory_id = $this.attr('data-subcategoryId');
	if(subcategory_id!=''){
		$.ajax({
			url: getAjaxUrl() + "subcategory.php",
			type: "POST",
			crossDomain:true,
			data: {"guid": getLocalStorageItem("userid"),'session_id':getSessionId(), 'subcategory_id':subcategory_id},
			beforeSend: function(){
				
			},
			success: function(data){
				setSubcategoryView(data);
			},
			error: function(xhr, status, error){
				console.log(xhr.responseText);
			},
			complete: function(){
				
			}
		});
	}
});

function setSubcategoryView(data){
	var obj = $.parseJSON(data);
	var html='<div class="ad-title"> <h2>'+obj['subcategory']+' <span class="ad-page-price"> <a>'+obj['count']+' ads found</a> </span> </h2> </div><section class="container m-t-60"><div class="row"> <div class="col-md-9 col-sm-12 col-xs-12"> <section id="ads-homepage" class="category-page-ads clearfix"> <div class="row"> <div class="tab-content"> <div class="tab-pane active fade in latest-ads-holder" id="Tlatest"> <div class="latest-ads-grid-holder">';
	var ads = obj['ads'];
	if($.isEmptyObject(obj.ads)){
		html+='<h1>No Ads Found</h1>';
	}
	else{
		for(i=0;ads[i]!=null;i++){
			var ad = ads[i];
			var image = (ad['image']=='' || ad['image']==null) ? "images/noimg.jpg":ad['image'];
			html+='<div class="col-md-4 col-sm-6 col-xs-12" id="adv'+ad['ads_id']+'"> <div class="ad-box random-posts-grid "> <a class="ad-image subcategory-grid" data-id="'+ad['ads_id']+'" href="#" title=""> <img width="270" height="220" src="'+image+'" class="attachment-270x220 size-270x220 wp-post-image" sizes="(max-width: 270px) 100vw, 270px"> </a> <div class="post-title-cat"> <div class="post-title subcategory-grid" data-id="'+ad['ads_id']+'" style="display: inline-block;"> <div class="post-title-icon" style="background-color:'+getRandomColor()+'"><i class="fa fa-book"></i></div> <a href="#">'+ad['title']+'</a> </div> <div class="delete-ad-grid" style="display: inline-block;"> ';
			if(ad['posted_by']==getLocalStorageItem('userid') || obj.isAdmin){
				html+='<i class="fa fa-trash" onclick=confirmOp("deleteThisAd('+ad['ads_id']+')")></i>';
			}
			html+=' </div> </div> </div> </div>';
			
		}
	}
	html+='</div> </div> </div> </div> </section> </div> </div></section>';
	$('#content_area').html(html);
}

/*
	FUNCTION TO LOAD ADVERTISEMENT VIEW
*/
$(document).on('click', '.subcategory-grid:not(.delete-ad-grid)', function(event){
	otherPage = true;
	event.preventDefault();
	var $this=$(this);
	var ads_id = $this.attr('data-id');
	getAd(ads_id);
	
});

function getAd(ads_id){
	if(ads_id!=''){
		$.ajax({
			url: getAjaxUrl() + "advertisement.php",
			type: "POST",
			crossDomain:true,
			data: {"guid": getLocalStorageItem("userid"),'session_id':getSessionId(), 'ads_id':ads_id},
			beforeSend: function(){
				
			},
			success: function(data){
				setAdView(data);
			},
			error: function(xhr, status, error){
				console.log(xhr.responseText);
			},
			complete: function(){
				
			}
		});
	}
}

function setAdView(data){
	var obj = $.parseJSON(data);
	if(obj.status==400){
		alert('Oops!Something went wrong');
		loadHome();
		return false;
	}
	var ad = obj['ad'];
	if(ad['image']==null || ad['image']=='')
		ad['image'] = 'images/noimg.jpg';
	var settings = obj['settings'];
	var posted = obj['posted'];
	var html ='<div class="ad-title" style="margin-bottom:0px;padding: 1px 0px !important;background:rgb(255, 214, 0)"> <h2 style="line-height: 0px !important;">'+ad['title']+'</h2> </div>';
	html+='<section class="container"> <div class="row"> <div class="col-md-12"> <div class="single-slider"> <img style="margin-bottom:-7px;width:100%;" src="'+ad['image']+'">';
	if(ad['price']!=null && ad['price']!=''){
		html+='<div class="single-ad-price">Qty. <!--<img src="images/rs.png" style="width: 14px;margin-top: -2px;margin-right: 5px;">-->'+ad['price']+'</div>';
	}
	html+=' <div class="clearfix"></div> </div> <input type="hidden" id="posted_by" value="'+ad['posted_by']+'">';
	if(ad['video']!=null && ad['video']!=''){
		html+='<div id="ab-video-text"><span><i class="fa fa-youtube-play"></i>Video</span></div> <div id="ab-video"> <div class="fluid-width-video-wrapper" style="padding-top: 56.2069%;"> <iframe width="854" height="480" src="'+ad['video']+'" frameborder="0" allowfullscreen=""></iframe> </div> </div>';
	}
	html+=' <div class="post-detail"> <div class="detail-cat clearfix" style="padding:5px !important;margin-top: 8px;"> <div class="category-icon"> <div class="category-icon-box"><i class="fa fa-book"></i></div> </div> <a href="#">'+ad['title']+'</a> </div> <hr style="margin-top:0px;margin-bottom:0px">';
	if(ad['description']!=null && ad['description']!=""){
		html+='<div class="single-description" style="padding: 0px 5px 0px;"> <div class="description-title" style="margin-bottom: 5px !important;">DESCRIPTION </div> <p>'+ad['description']+'</p> </div>';
	}
	html+='<div class="clearfix"></div> </div> <div class="clearfix"></div> <div class="author-info clearfix post-single"> <div class="author-avatar" style="margin-bottom: 0px;"> <a href="#"> <img src="'+getBaseUrl()+'/avatar/'+posted['username']+'/larger" style="border-radius: 12%;"> </a> </div> <div class="author-detail-right clearfix"> <span class="author-meta" style="margin-bottom: 15px;"> <i class="fa fa-location-arrow"></i>Location:&nbsp; '+ad['city']+'</span> <span class="author-meta" style="margin-bottom: 15px;"> <i class="fa fa-map-marker"></i>Address:&nbsp; '+ad['address']+'</span> <span class="author-meta" style="margin-bottom: 15px;"> <i class="fa fa-phone"></i>Phone:&nbsp; '+settings['phone']+'</span> <span class="author-meta" style="margin-bottom: 15px;"> <i class="fa fa-envelope"></i>Email:&nbsp; '+posted['email']+' </span> </div> <div style="display: none"class="author-btn"> <span class="author-profile-ad-details"><a href="#" class="button-ag large green">VIEW '+posted['first_name']+' '+posted['last_name']+'&#39;s PROFILE</a></span> </div> </div> <div class="ad-detail-content"> </div>';
	
	if(getLocalStorageItem('userid')!=null && getLocalStorageItem('userid')!=posted['guid']){
		html+='<div class="full author-form full-single"> <div class="pos-relative"> <h3 style="margin-top:0px;padding-bottom:0px;margin:0px 0px">LEAVE MESSAGE TO '+posted['first_name'].toUpperCase()+' '+posted['last_name'].toUpperCase()+'</h3> <div class="h3-seprator" style="margin-bottom: 10px;"></div> </div> <div id="contact-ad-owner-v2" style="margin-bottom:5px"> <div class="col-md-12 col-xs-12"> <textarea class="form-control" placeholder="Write your request here... (Add your school address)" name="comments" id="commentsText" cols="8" rows="5"></textarea> </div> <div class="col-md-12 col-xs-12"> <span id="contact-ad-owner-error" style="margin-left:25px;display:none"></span><input name="submitted" type="submit" value="Send Messenger" class="input-submit" onclick="sendMessageApp()"> </div> </div> </div>';
	}
	if(getLocalStorageItem('userid')==posted['guid']){
		html+='<button type="button" class="btn btn-danger" style="float:right;margin-top:10px;padding: 2px 5px; margin-left:5px" onclick=confirmOp("deleteThisAd('+ad['ads_id']+')")> <i class="fa fa-trash" aria-hidden="true"></i>&nbsp; </button> <a href="#"> <button type="button" class="btn btn-success" style="float:right;margin-top:10px;padding: 2px 5px;" onclick="newAd('+ad['ads_id']+')"> <i class="fa fa-pencil" aria-hidden="true"></i>&nbsp; </button> </a> ';
	}
html+='<!--<div id="ad-comments"> <div id="ad-comments"> </div> </div>--> </div> </div> </section>';
	
	html+='';
	$('#content_area').html(html);
}
/*
	FUNCTION TO LOAD CATEGORIES
*/
function loadCategories(){
	otherPage = true;
	closeMenu();
	$.ajax({
		url: getAjaxUrl() + "cats.php",
		type: "POST",
		crossDomain:true,
		data: {'session_id':getSessionId()},
		beforeSend: function(){
			
		},
		success: function(data){
			setCatsView(data);
		},
		error: function(xhr, status, error){
			console.log(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}
function setCatsView(data){
	var html ='<div class="container"> <div class="section-header-style-1"> <div class="row"> <div class="col-xs-12"> <h4 class="main-title" style="margin-top: 3px;text-align: center;margin-bottom: 0px;color: #f19659;">SERVICE CATEGORIES</h4> <div class="main-sub-title"> </div> <div class="h2-seprator"></div> </div> </div> </div> <div class="row cat-wrapper" data-call="11"> <div class="col-md-12 category-box-outter" id="cat-26"> <div class="category-box"> <div class="category-content clearfix"> <ul class="append-li"> ';
	var obj = $.parseJSON(data);
	for(var i=0;obj[i]!=null;i++){
		var cat = obj[i];
		html+='<li style="font-size:16px !important;color: silver" onclick=loadAdsByCategory('+cat['id']+')> '+cat['cat']+' <span style="color:red" class="category-counter">'+cat['count']+'</span> </li>';
	}
	html+=' </ul> </div> <div class="clearfix"></div> </div> </div> </div> </div>';
	$('#content_area').html(html);
}

/*
	LOAD POSTS BY CATEGORY
*/
function loadAdsByCategory(cat_id){
	$.ajax({
		url: getAjaxUrl() + "category_posts.php",
		type: "POST",
		crossDomain:true,
		data: {'session_id':getSessionId(), 'cat_id':cat_id},
		beforeSend: function(){
			
		},
		success: function(data){
			setCategoryPostsView(data);
		},
		error: function(xhr, status, error){
			console.log(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}
function setCategoryPostsView(data){
	var obj = $.parseJSON(data);
	var html = '<section id="categories-homepage" class="cat-box-contain" style="background-color:#ffffff;"> <div class="container"> <div class="section-header-style-1"> <div class="row"> <div class="col-xs-12"> <h2 class="main-title" style="margin-top: 5px;font-size: 20px !important;">SERVICES IN '+obj.category['category'].toUpperCase()+'</h2> <div class="main-sub-title"> </div> <div class="h2-seprator"></div> </div> </div> </div><div class="row cat-wrapper" data-call="11">';
	
	if(obj.status==400){
		alert(obj.message);
		return false;
	}
	html+='<section class="container"> <div class="row"> <div class="col-md-9 col-sm-12 col-xs-12"> <section id="ads-homepage" class="category-page-ads clearfix"> <div class="row"> <div class="tab-content"> <div class="tab-pane active fade in latest-ads-holder" id="Tlatest"> <div class="latest-ads-grid-holder">';
		if($.isEmptyObject(obj.ads)){
			html += '<div class="col-md-4 col-sm-6 col-xs-12"><h1> No Services Found</h1></div>';
		}
		for(var i=0; obj.ads[i]!=null;i++){
			var ad = obj.ads[i];
			ad['image'] = ad['image']==null ? "images/noimg.jpg": ad['image'];
			html+='<div class="col-md-4 col-sm-6 col-xs-12" id="adv'+ad['ads_id']+'"> <div class="ad-box random-posts-grid "> <a class="ad-image subcategory-grid" data-id='+ad['ads_id']+' href="#" title=""> <img width="270" height="220" src="'+ad['image']+'" class="attachment-270x220 size-270x220 wp-post-image" sizes="(max-width: 270px) 100vw, 270px"> </a>';
			if(ad['price']!=null && ad['price']!=''){
				html+='<div class="add-price"><span> <img src="images/rs.png" style="width: 8px;margin-top: -1px;"> 1500 </span> </div>';
		  	}
            html+='<div class="post-title-cat"> <div class="post-title subcategory-grid" data-id='+ad['ads_id']+' style="display: inline-block;"> <div class="post-title-icon" style="background-color:#53cc37"><i class="fa fa-book"></i></div> <a href="#"> '+ad['title']+'</a> </div>';
			if(ad['posted_by']==obj['logged_in'] || obj.isAdmin ){
				html+='<div class="delete-ad-grid" style="display: inline-block;" onclick=confirmOp("deleteThisAd('+ad['ads_id']+')")> <i class="fa fa-trash"></i> </div>';
			}
		html+='</div> </div> </div>';
		}
		html+='</div> </div> </div> </div> </section> </div> </div> </section>'
	$('#content_area').html(html);
	
}

/*
	FUNCTION TO LOAD USER PROFILE
*/

function loadProfile(){
	otherPage = true;
	closeMenu();
	$.ajax({
		url: getAjaxUrl() + "profile.php",
		type: "POST",
		crossDomain:true,
		data: {'session_id':getSessionId()},
		beforeSend: function(){
			
		},
		success: function(data){
			setProfileView(data);
		},
		error: function(xhr, status, error){
			console.log(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}
function setProfileView(data){
	var obj = $.parseJSON(data);
	var user = obj['user'];
	var settings = obj['settings'];
	settings['phone'] = settings['phone']==null ? "" : settings['phone'];
	var ads = obj['ads'];
	var html = '<div class="ad-title" style="margin-bottom: 10px;margin: 0px 0;padding:0px 0px;background: #ffd600;"><h2 style="margin: 0px 0;line-height: 45px !important;">Face</h2></div>';
	
	html+='<section class="container m-t-60" style="margin-top:0px"> <div class="row"> <div class="col-md-12"> <div class="full"> <div class="account-overview clearfix" style="margin-bottom:0px"> <div class="col-sm-4 col-xs-12 first author-avatar-edit-post p-30" style="padding-top:0px; padding-bottom:0px;padding: 5px;"> <img style="width:80px;height:80px" class="author-avatar" src="'+user['iconURLS']['larger']+'"> </div> <div class="col-sm-8 col-xs-12 b-l-1 identifier-span" style="text-align:left">  <span class="author-cc show-email" style="padding: 0px;" > <i class="fa ';
	if(settings['show_email']==0)
		html+='fa-times" style="background:red"';
	else
		html+='fa-check"';
	html+=' onclick="updateSettingsApp(&quot;email&quot;);" title="Click to change settings"></i> Show Email in My Ads </span> <span class="author-cc show-phone" style="padding: 0px;"> <i class="fa ';
	if(settings['show_phone']==0)
		html+='fa-times" style="background:red"';
	else
		html+='fa-check"';
	html+=' onclick=updateSettingsApp("phone") title="Click to change settings"></i> Show Phone in My Services </span> <span class="author-details phone-number" style="margin-bottom:12px"><i class="fa fa-phone" style="background:silver"></i>Phone: <span style="display:inline-block" class="phone-text">'+settings['phone']+'</span> <input type="text" class="form-control edit-phone-box" onfocus="this.value = this.value;" value="'+settings['phone']+'" style="width:180px;display:none" onblur=updateSettingsApp("phone-num")> <i class="fa fa-pencil edit-phone" area-hidden="true" onclick="editPhone()"></i> </span> <span class="author-details"> <i class="fa fa-envelope" style="background:silver"></i>Email: '+user['email']+'</span></div> </div> <div class="clearfix"> <!--<h3 style="margin-top:0px;margin: 0px 0;margin-top: 10px;padding-bottom: 0px;">AUTHOR PROFILE</h3> <div class="h3-seprator" style="margin-bottom: 10px;"></div>--> <!--<div class="full profile-content"> <div class="col-xs-12"> <span class="author-details phone-number" style="margin-bottom: 12px;"><i class="fa fa-phone" style="background: silver;"></i>Phone: <span style="display:inline-block" class="phone-text">'+settings['phone']+'</span> <input type="text" class="form-control edit-phone-box" onfocus="this.value = this.value;" value="'+settings['phone']+'" style="width:180px;display:none" onblur=updateSettingsApp("phone-num")> <i class="fa fa-pencil edit-phone" area-hidden="true" onclick="editPhone()"></i> </span> <span class="author-details"> <i class="fa fa-envelope" style="background: silver;"></i>Email: '+user['email']+'</span> </div> </div> --></div> </div> <div class="full"> <h3 style="margin-top: 0px;padding-bottom: 0px;color: #9f00ff;">MY SERVICES</h3> <div class="h3-seprator" style="margin-bottom: 0px; */"></div> <div class="full" style="margin-left: 0px; padding-top: 3px;">';
	for(var i=0;ads[i]!=null;i++){
		var ad = ads[i];
		if(ad['image']==null || ad['image']=='')
			ad['image']='images/noimg.jpg';
		html+='<div class="my-ad-box clearfix"><div class="my-ad-image" style="width:100%;text-align:center" onclick=getAd('+ad['ads_id']+')> <img src="'+ad['image']+'" style="max-width: 70%;"> </div> <div class="my-ad-details" style="padding-top:0px"> <div class="post-title-my-ad clearfix"> <div class="post-title" style="margin:2px"> <a href="#"> <span style="margin-right: 0px;font-size:11px" class="title-span"onclick=getAd('+ad['ads_id']+')><i class="fa fa-cog" style="color:'+getRandomColor()+';line-height: 33px;margin-left: 5px;"></i></span> <span style="font-size:16px" onclick=getAd('+ad['ads_id']+')>'+ad['title']+'</span><br></a> <span style="font-size: 13px;"><i class="fa fa-clock-o"></i> '+ad['created']+'</span> <span class="my-ad-a" style="margin-right: 5px;"> <a href="#" style="margin-right:0px !important"><i class="fa fa-pencil edit-ad" onclick="newAd('+ad['ads_id']+')"></i> </a> <i class="fa fa-trash-o delete-ad" onclick=confirmOp("deleteThisAd('+ad['ads_id']+')")></i> </span> </div> </div> </div> </div>';
	}
	html+='</div> </div> </div> </div> </section>';
	$('#content_area').html(html);
}

/*
	FUNCTION FOR ADDING NEW ADVERTISEMENT
*/
function newAd(ad_id){
	otherPage = true;
	$.ajax({
		url: getAjaxUrl() + "newAd.php",
		type: "POST",
		crossDomain:true,
		data: {'session_id':getSessionId(), 'ad_id':ad_id},
		beforeSend: function(){
			
		},
		success: function(data){
			setNewAdView(data);
		},
		error: function(xhr, status, error){
			console.log(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}
function setNewAdView(data){
	var obj = $.parseJSON(data);
	var ad = obj['ad'];
	var categories = obj['categories'];
	var title = ad['title']==undefined ? "" : ad['title'];
	var ads_id = ad['ads_id']==undefined ? "" : ad['ads_id'];
	var aid = ad['id']==undefined ? "" : ad['id'];
	ad['city'] = ad['city']==undefined ? "" : ad['city'];
	ad['address'] = ad['address']==undefined ? "" : ad['address'];
	ad['description'] = ad['description']==undefined ? "" : ad['description'];
	var page_title = ad['id']==undefined ? "NEW SERVICE" : "EDIT SERVICE";
	var button_title = ad['id']==undefined ? "PUBLISH SERVICE" : "SAVE SERVICE";
	var html = '<div class="ad-title" style="margin-bottom: 10px;background:rgb(255, 214, 0)"><h2 style="margin: 0px 0;line-height: 30px !important;">'+page_title+'</h2></div>';
	html+='<section class="container"> <div class="row"> <div class="col-md-9 col-sm-12 col-xs-12"> <div class="col-sm-9 col-xs-12 first ad-post-main"> <div id="upload-ad" class="ad-detail-content"> <form class="form-item row" id="primaryPostFormApp" method="POST" enctype="multipart/form-data"> <div class="col-xs-12" style="display:none"> <h3 style="margin-top:-6px;"> MAKE NEW SERVICE </h3> <div class="h3-seprator" style="margin-bottom: 10px;"></div> </div><input type="hidden" id="hidden_id" name="ads_id" value="'+ads_id+'"> <input type="hidden" id="hidden_adsid" name="id" value="'+aid+'"> <div class="col-xs-12"> </div> <div class="col-sm-6 col-xs-12" style="width:100% !important"> <input type="text" id="postTitle" name="title" placeholder="Title Goes here |" size="60" maxlength="255" class="form-control" required="" value="'+title+'"> </div> <div class="col-sm-6 col-xs-12 form-s-select" style="width:100% !important"> <select name="cat_id" id="cateID" class="mainCategory" style="display: block; width:100%; margin-bottom:10px; border-radius:60px;height:44px;border:1px solid #eee"><option value="-1">--Select Category--</option>';
	 for(var i=0;categories[i]!=null;i++){
		 var cat = categories[i];
		 if(cat['id']==ad['cat_id'])
			html+='<option class="level-0" value="'+cat['id']+'" selected="selected">'+cat['category']+'</option>';
		 else
			html+='<option class="level-0" value="'+cat['id']+'">'+cat['category']+'</option>';
	 }
					 
	 html+='</select></div> <div class="extra-fields-container"> </div> <div class="col-sm-6 col-xs-12"> <input type="number" id="post_price" name="price" onkeypress="return isNumber(event)" placeholder="Quantity" size="30" class="form-control" value="'+ad['price']+'"> </div> <div class="col-sm-6 col-xs-12 form-s-select"> <input type="text" id="post_location" name="city" placeholder="Location" size="30" class="form-control" value="'+ad['city']+'"> </div> <div class="col-xs-12 form-s-select"> <input type="text" id="post_location" name="address" placeholder="Address" size="30" class="form-control" value="'+ad['address']+'"> </div> <div class="col-xs-12"> <fieldset class="input-title" style="margin-bottom:0px;"> <textarea style="height: 150px !important;;border:1px solid #cecdcd" name="description" id="video" cols="8" rows="5" placeholder="Service Description Goes here |">'+ad['description']+'</textarea> </fieldset> </div>';
	if(ad['image']!=null && ad['image']!='' && ad['image']!=undefined){
		html+='<div class="col-xs-12"> <img style="width:100%" src="'+ad['image']+'"> <input type="hidden" name="image" value="'+ad['image']+'"> </div>';
	}
	html+=' <div class="col-xs-12" align="center"> <div class="jfiler-holder"> <div class="jFiler jFiler-theme-dragdropbox"> <input type="file" name="file" id="filer_input" multiple="" style="position: absolute; left: -9999px; top: -9999px; z-index: -9999;"> <div class="jFiler-input-dragDrop" onclick="openFileChoose()"> <div class="jFiler-input-inner"> <div class="jFiler-input-text"> <h4>Click here to upload files</h4> </div> <a class="jFiler-input-choose-btn" onclick="">Browse Images</a> </div> </div> </div> </div> </div> <div class="col-xs-12"> <fieldset class="input-title" style="margin-bottom:0px;"> <textarea name="video" id="video" cols="8" rows="2" placeholder="Put video url here."></textarea> <p class="help-block">Add video embedding URL here (youtube only)</p> </fieldset> </div> <div class="col-xs-12"> <div class="publish-ad-button"> <button class="btn form-submit full-btn" id="edit-submit" name="op" value="" type="submit"> '+button_title+' </button> </div> </div> </form> </div> </div> </div> </div> </section>';
	$('#content_area').html(html);
}

function chooseSubCategory(){
	var value = $('.mainCategory option:selected').val();
	if(value=='-1')
		return false;
	$('#cateID').css('border', '1px solid #eee');
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'newad-subcat', 'value':value, 'session_id':getSessionId()},
		success: function(data){
			var obj = $.parseJSON(data);
			var html = "<option value='-1'>--Select Subcategory--</option>";
			for(var i = 0; obj[i]!=null; i++){
				var subcat=obj[i];
				html+="<option value='"+subcat['id']+"'>"+subcat['subcategory']+"</option>";
			}
			$('.subCategory').html(html);
		},
		error:function(req, err,error){
			
		}
	});
}

/*
	NEW AD SUBMIT HANDLER
*/
$(document).on('submit', '#primaryPostFormApp', function(event){
	event.preventDefault();
	if($('#postTitle').val()==''){
		$($('#postTitle').focus())
		return false;
	}
	if($('#cateID option:selected').val()=='-1'){
		$('#cateID').css('border', '1px solid rgba(0, 115, 168, 0.45)');
		$('html, body').animate({
			scrollTop: ($("#cateID").offset().top) - 15
		}, 200);
		return false;
	}
	else{
		$('#cateID').css('border', '1px solid #eee');
	}
	var formData = new FormData($(this)[0]);
	saveAd(formData);
});

function saveAd(formData){
	formData.append('session_id', getSessionId());
	formData.append('action', 'newad-add');
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: formData,
		processData:false,
		contentType:false,
		success: function(data){
			setAdView(data);
		},
		error:function(req, err,error){
			
		}
	});
}

/*
	FUNCTION TO VIEW ADS BASED ON CATEGORY
*/
$(document).on('click', '.category-viewall', function(event){
	event.preventDefault();
	var cat_id = $(this).attr('data-id');
	$.ajax({
		url: getAjaxUrl() + "category.php",
		type: "post",
		data: {'session_id':getSessionId(), "cat_id":cat_id},
		success: function(data){
			setCategoryView(data);
		},
		error:function(req, err,error){
			
		}
	});
});

function setCategoryView(data){
	var obj = $.parseJSON(data);
	var html = '<div class="ad-title"> <h2>'+obj['category']+'<span class="ad-page-price"> <a>'+obj['ads_count']+' ads found</a> </span> </h2> </div>';
	if(obj['ads_count']==0){
		html+='<h1>No Ads Found</h1>';
	}
	else{
		html+='<section class="container m-t-60"> <div class="row"> <div class="col-md-9 col-sm-12 col-xs-12"> <section id="ads-homepage" class="category-page-ads clearfix"> <div class="row"> <div class="tab-content"> <div class="tab-pane active fade in latest-ads-holder" id="Tlatest"> <div class="latest-ads-grid-holder">';
		for(var i=0; obj.ads[i]!=null;i++){
			var ad = obj.ads[i];
			ad['image'] = ad['image']==null ? "images/noimg.jpg": ad['image'];
			html+='<div class="col-md-4 col-sm-6 col-xs-12" id="adv'+ad['ads_id']+'"> <div class="ad-box random-posts-grid "> <a class="ad-image subcategory-grid" data-id='+ad['ads_id']+' href="#" title=""> <img width="270" height="220" src="'+ad['image']+'" class="attachment-270x220 size-270x220 wp-post-image" sizes="(max-width: 270px) 100vw, 270px"> </a>';
			if(ad['price']!=null && ad['price']!=''){
				html+='<div class="add-price"><span> <img src="images/rs.png" style="width: 8px;margin-top: -1px;"> 1500 </span> </div>';
		  	}
            html+='<div class="post-title-cat"> <div class="post-title subcategory-grid" data-id='+ad['ads_id']+' style="display: inline-block;"> <div class="post-title-icon" style="background-color:#53cc37"><i class="fa fa-book"></i></div> <a href="#"> '+ad['title']+'</a> </div>';
			if(ad['posted_by']==obj['logged_in'] || obj.isAdmin ){
				html+='<div class="delete-ad-grid" style="display: inline-block;" onclick=confirmOp("deleteThisAd('+ad['ads_id']+')")> <i class="fa fa-trash"></i> </div>';
			}
		html+='</div> </div> </div>';
		}
		html+='</div> </div> </div> </div> </section> </div> </div> </section>'
	}
	$('#content_area').html(html);
}
function deleteThisAd(id){
	if(id=='')
		return false;
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'ad-delete', 'id':id,'session_id':getSessionId()},
		success: function(data){
			var response = $.parseJSON(data);
			if(response.code==200){
				if($('#adv'+id).length)
					$('#adv'+id).remove();
				else
					loadHome();
			}
			else{
				alert('Oops! Something went wrong');
			}
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			$('#no-btn').click();
		}
	});
}

function updateSettingsApp(which){
	var show_phone;
	var show_email;
	var phone=$('.phone-text').html().trim();
	if(which=='email'){
		if($('.author-cc.show-email>i').hasClass('fa-times'))
			show_email=1;
		else
			show_email=0;
		
		if($('.author-cc.show-phone>i').hasClass('fa-times'))
			show_phone=0;
		else
			show_phone=1;
	}
	else if(which=='phone'){
		if($('.author-cc.show-phone>i').hasClass('fa-times'))
			show_phone=1;
		else
			show_phone=0;
		if($('.author-cc.show-email>i').hasClass('fa-times'))
			show_email=0;
		else
			show_email=1;
	}
	else if(which=='phone-num'){
		phone = $('.edit-phone-box').val();
		if($('.author-cc.show-phone>i').hasClass('fa-times'))
			show_phone=0;
		else
			show_phone=1;
		if($('.author-cc.show-email>i').hasClass('fa-times'))
			show_email=0;
		else
			show_email=1;
	}
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'session_id':getSessionId(), 'action':'settings', 'show_phone':show_phone,'show_email':show_email, 'phone':phone},
		success: function(data){
			setProfileView(data);
		},
		error:function(req, err,error){
			
		},
		complete:function(){
			
		}
	});
}

/*
	LOAD ADMIN VIEW
*/

function loadAdmin(){
	otherPage = true;
	closeMenu();
	$.ajax({
		url: getAjaxUrl() + "admin.php",
		type: "post",
		data: {'session_id':getSessionId()},
		success: function(data){
			setAdminView(data);
		},
		error:function(req, err,error){
			
		},
		complete:function(){
			
		}
	});
}
function setAdminView(data){
	var obj = $.parseJSON(data);
	if(obj.status==400){
		alert(obj.message);
		return false;
	}
	var html = '<div class="ad-title" style="margin-bottom:10px"><h2>ADMIN</h2></div>';
	html+='<section class="container m-t-60" style="margin-top: 0px;"> <div class="row"> <div class="col-md-9 col-sm-12 col-xs-12 admin-wrapper" style="width:100% !important;"> <h3 style="margin-top:-5px">CATEGORIES</h3> <div class="h3-seprator"></div> <div class="account-overview clearfix"> <div class="col-xs-12 first author-avatar-edit-post p-30" style="padding-bottom:15px !important">';
	for(var i=0;obj.categories[i]!=null; i++){
		var category = obj.categories[i];
		html+='<span class="author-details phone-number"> <i class="fa fa-cog" style="background:'+getRandomColor()+';margin-right: 0px;margin-left: -25px;"></i> <span style="display:inline-block" class="category-text" title="Click to see more" id="cat_span'+category['id']+'" onclick="viewSubCategoryApp('+category['id']+')">'+category['category']+'</span> <input type="text" class="form-control edit-category-box" id="cat'+category['id']+'" value="'+category['category']+'" onblur="updateCategoryApp('+category['id']+')" onfocus="this.value=this.value"> <span style="float:right"> <i class="fa fa-pencil icon-cat" area-hidden="true" onclick="editCategory('+category['id']+')"></i> <i class="fa fa-times icon-cat" area-hidden="true" onclick=confirmOp("deleteCategoryApp('+category['id']+')")></i> </span> </span>';
	}
	html+='<input type="text" class="form-control new-category" placeholder="Category Title |" onblur="saveCategoryApp(this.value)"> <i class="fa fa-times close-new-cat" onclick="closeNewCat()"></i> <span class="add-cat-error"></span> <span style="float:right"> <button type="button" id="add-category" class="btn btn-primary add-category" onclick="newCategory()"> <i class="fa fa-plus-circle"></i>&nbsp;&nbsp; Add New </button> </span> </div> </div> </div> </div> </section>';
	$('#content_area').html(html);
}

/*
	UPDATE CATEGORY
*/
function updateCategoryApp(id){
	var cat_name = $('#cat'+id).val();
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'cat-update', 'category':cat_name,'id':id, 'session_id':getSessionId()},
		success: function(data){
			setAdminView(data);
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}

/*
	DELETE CATEGORY
*/
function deleteCategoryApp(id){
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'cat-delete', 'id':id,'session_id':getSessionId()},
		success: function(data){
			var obj = $.parseJSON(data);
			if(obj.code==200){
				setAdminView(data);
			}
			else
				alert(obj.Message);
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			$('#no-btn').click();
		}
	});
}

/*
	SAVE CATEGORY
*/
function saveCategoryApp(category){
	if(category=='')
		return false;
	clearError();
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'cat-add', 'category':category, 'session_id':getSessionId()},
		success: function(data){
			var obj = $.parseJSON(data);
			if(obj.code!=200){
				$('.add-cat-error').html(obj.Message)
								   .fadeIn(200)
								   .delay(5000)
								   .fadeOut();
								   
			}
			else{
				setAdminView(data);
			}
				
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}

/*
	VIEW SUBCATEGORIES
*/
var category_html = '';
function viewSubCategoryApp(id){
	if(id=='')
		return false;
	category_html=$('.admin-wrapper').html();
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'subcat-view', 'id':id, 'session_id':getSessionId()},
		success: function(data){
			var obj = $.parseJSON(data);
			var cat_name = obj.cat['category'];
			var html = '<h3 style="margin-top:-5px">'+cat_name.toUpperCase()+' > SUBCATEGORIES</h3><div class="h3-seprator"></div><span class="back-btn" onclick="backToCat()"><i class="fa fa-arrow-circle-left"></i>Back</span><div class="account-overview clearfix"><div class="col-xs-12 first author-avatar-edit-post p-30" style="padding-bottom:15px !important">';
			for(var i =0; obj.subc[i]!=null;i++){
				var $this = obj.subc[i];
				html+='<span class="author-details phone-number" id="subcat-wrapper'+$this['id']+'"><i class="fa fa-cog" style="background: '+getRandomColor()+';"></i><span style="display:inline-block" class="sub-category-text"  id="subcat_span'+$this['id']+'">'+$this['subcategory']+'</span><input type="text" class="form-control edit-subcategory-box" id="subcat'+$this['id']+'" value="'+$this['subcategory']+'" onblur="updateSubCategoryApp('+$this['id']+','+$this['category_id']+')" onfocus="this.value=this.value"><span style="float:right"><i class="fa fa-pencil icon-cat" area-hidden="true" onclick="editSubCategory('+$this['id']+')"></i><i class="fa fa-times icon-cat" area-hidden="true" onclick=confirmOp("deletetSubCategoryApp('+$this['id']+')")></i></span></span>';
			}
               	
           	html+= '<input type="text" class="form-control new-subcategory" placeholder="Subcategory Title |" onblur=saveSubCategoryApp(this.value,'+obj.cat['id']+')><i class="fa fa-times close-new-subcat" onClick="closeNewSubCat()"></i><span class="add-subcat-error"></span><span style="float:right"><button type="button" id="add-subcategory" class="btn btn-primary add-subcategory" onClick="newSubCategory()"><i class="fa fa-plus-circle"></i>&nbsp;&nbsp;Add New</button></span></div></div>';
			
			$('.admin-wrapper').html(html);
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}
/*
	UPDATE SUBCATEGORY
*/
function updateSubCategoryApp(id,cat_id){
	var subcat_name = $('#subcat'+id).val();
	if($('#subcat_span'+id).html()==subcat_name){
		clearEdit(id);
		return false;
	}
	else if(subcat_name==''){
		clearEdit(id);
		return false;
	}
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'subcat-update', 'subcategory':subcat_name,'id':id, 'category_id':cat_id, 'session_id':getSessionId()},
		success: function(data){
			var obj = $.parseJSON(data);
			if(obj.code==200){
				$('#subcat_span'+id).html(subcat_name);
			}
			clearEdit(id);
			alert(obj.Message);
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}
/*
	SAVE SUBCATEGORY
*/

function saveSubCategoryApp(subcategory, category_id){
	if(subcategory=='')
		return false;
	clearError();
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'subcat-add', 'subcategory':subcategory, 'category_id':category_id, 'session_id':getSessionId()},
		success: function(data){
			var obj = $.parseJSON(data);
			if(obj.code==201){
				$('.add-subcat-error').html(obj.Message)
								   	  .fadeIn(500)
								      .delay(5000)
								      .fadeOut();
								   
			}
			else if(obj.code==200){
				$this=obj.data;
				if($this.length==0)
					loadAdmin();
				var html='<span class="author-details phone-number" id="subcat-wrapper'+$this['id']+'"><i class="fa fa-cog" style="background: '+getRandomColor()+';"></i><span style="display:inline-block" class="sub-category-text"  id="subcat_span'+$this['id']+'">'+$this['subcategory']+'</span><input type="text" class="form-control edit-subcategory-box" id="subcat'+$this['id']+'" value="'+$this['subcategory']+'" onblur="updateSubCategoryApp('+$this['id']+','+$this['category_id']+')" onfocus="this.value=this.value"><span style="float:right"><i class="fa fa-pencil icon-cat" area-hidden="true" onclick="editSubCategory('+$this['id']+')"></i><i class="fa fa-times icon-cat" area-hidden="true" onclick=confirmOp("deletetSubCategoryApp('+$this['id']+')")></i></span></span>';
				var ele = $('.col-xs-12.first.author-avatar-edit-post.p-30').find('.author-details.phone-number:last');
				if(ele.length)
					$('.col-xs-12.first.author-avatar-edit-post.p-30').find('.author-details.phone-number:last').after(html);
				else
					$('.col-xs-12.first.author-avatar-edit-post.p-30').find('.new-subcategory').before(html);
				$('.new-subcategory').val('');
				closeNewSubCat();
				
			}
			alert(obj.Message)
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			
		}
	});
}
/*
	DELETE SUBCATEGORY
*/
function deletetSubCategoryApp(id){
	if(id=='')
		return false;
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'subcat-delete', 'id':id, 'session_id':getSessionId()},
		success: function(data){
			var obj = $.parseJSON(data);
			if(obj.code==200){
				$('#subcat-wrapper'+id).remove();
			}
			alert(obj.Message);
		},
		error:function(req, err,error){
			console.log('er');
		},
		complete:function(){
			$('#no-btn').click();
		}
	});
}

/*
	SEND MESSAGE TO AD OWNER
*/
function sendMessageApp(){
	$('#contact-ad-owner-error').html('').hide();
	
	var message_to = $('#posted_by').val();
	var message = $('#commentsText').val();
	if(message_to=='')
		return false;
	if(message==''){
		$('#commentsText').focus();
		return false;
	}
	$.ajax({
		url: getAjaxUrl() + "xhr/xhr.php",
		type: "post",
		data: {'action':'contact-poster', 'message_to':message_to, 'message':message, 'session_id':getSessionId()},
		success: function(data){
			if(data==1){
				$('#contact-ad-owner-error').html('Message successfully sent')
											.css('color', 'green')
											.fadeIn(500)
											.delay(5000)
											.fadeOut()
											.css('color','red !important');
				$('#commentsText').val('');
			}
			else{
				$('#contact-ad-owner-error').html('Message could not be sent!')
										.fadeIn(500)
										.delay(5000)
										.fadeOut();
			}
		},
		error:function(req, err,error){
			console.log('er');
			$('#contact-ad-owner-error').html('Message could not be sent!')
										.css('color', 'red')
										.fadeIn(500)
										.delay(5000)
										.fadeOut();
		},
		complete:function(){
			
		}
	});
}
var otherPage = false;
function performBackAction(){
	if(!otherPage)
		parent.restoreHome();
	else{
		loadHome();
		otherPage = false;
	}
}