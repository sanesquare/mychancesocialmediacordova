/*$(document).on('click', '.flplayer', function(event){
	var $this = $(this).get(0);
	if($this.paused){
		$(this).next().hide();
		$(this).attr("controls", "true");
		if($this.currentTime == 15){
			$this.currentTime = 0;
		}
		$this.play();
	}
	else{
		$this.pause();
		$(this).removeAttr("controls");
		$(this).next().show();
	}
});*/
$(document).on('pause', 'video', function(event){
	$(this).next().show();
	$(this).attr("controls", "controls");
});
$(document).on('click', '.playpause', function(event){
	var $this = $(this).prev().get(0);
	if($this.paused){
		//parent.showFullscreen($(this).prev().find('source').attr('src'), 'video');
		parent.playVideo($(this).prev().find('source').attr('src'));
	}
	
	else{
		$(this).prev().removeAttr("controls");
		$this.pause();
		$(this).show();
	}
});