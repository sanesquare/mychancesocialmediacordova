/*
	Author: Suhail
*/


//override console writing; show log only for debug=true

var consoleHolder = console;
function debug(bool){
    if(!bool){
        consoleHolder = console;
        console = {};
        console.log = function(){};
    }else
        console = consoleHolder;
}
debug(false);

//ajax busy
var busy = false;
//set again=false, if data received was empty on scrolling; dynamic content loading
var again = true;

function resetGlobalvars(){
	busy = false;
	again = true;
	window.localStorage.setItem('offset', 1);
}

$(document).ready(function(e) {
    var funct = getLocalStorageItem('funct');
	if(funct!=null && funct!=''){
		eval(funct);
		removeLocalStorageItem('funct');
	}
	/*var i =0;
	setInterval(function(){
		console.log(i++);
		LoadHome(window.localStorage.getItem('offset'));
	}, 5000);*/
});

$(document).on('click', '.open-with-browser', function(event){
	event.preventDefault();
  	window.open($(event.currentTarget).attr('href'), '_system', '');
});

var username_soc = window.localStorage.getItem('userName');

function getBaseUrl(){
	var baseUrl = "http://m.mychance.in/";
	return baseUrl;
}
function getLocalStorageItem(key){
	return window.localStorage.getItem(key);
}

function removeLocalStorageItem(key){
	window.localStorage.removeItem(key);
}

function session_out(){
	removeLocalStorageItem('session_id');
}

function getSessionId(){
	//session_out();
	var session_id = getLocalStorageItem("session_id");
	if(session_id=='' || session_id===null || session_id==undefined){
		var userid = getLocalStorageItem('userid');
		var username = getLocalStorageItem("username");
		var hash_session = $.md5(userid+username);
		window.localStorage.setItem("session_id", hash_session);
		return $.md5(userid+username);
	}
	else
		return session_id;
}

function getParam(url ,name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

function disableClicks($this){
	$this.css('pointer-events', 'none')
		 .css('opacity', '0.5');
}

function enableClicks($this){
	$this.css('pointer-events', 'auto')
		 .css('opacity', '1');
}

function confirmOperation(){
	return confirm('Do you really want to perform this operation?');
}

$(document).on('ajaxStart', function() {
	$('.loader-wrapper').show();
	$('.no-loding').css('pointer-events', 'none')
				   .css('opacity', '0.4');
});

$(document).on('ajaxComplete', function() {
	$('.loader-wrapper').hide();
	$('.no-loding').css('pointer-events', 'auto')
				   .css('opacity', '1');
});

/*
*	Show posts based on scroll
**/

$(document).ready(function() {
	$(window).scroll(function() {
		if($('#comment-container').is(":visible"))
			return false;
		if($('.dropdown-menu').is(':visible'))
			$('.dropdown-menu').fadeOut(150);
		if($('#comment-container').is(':visible'))
			return false;
		if ($(window).scrollTop() + $(window).height() > $(".user-activity").height() && !busy && again) {
			var offset = parseInt(getLocalStorageItem('offset')) + 1;
			busy = true;
			var loaded_item = getLocalStorageItem('loaded_item');
			if(loaded_item=='loadhome')
				LoadHome(offset);
			else if(loaded_item=='loadusertimeline'){
				LoadUserTimeLine(username_soc ,offset);
			}
			else if(loaded_item=='performsearch')
				PerformSearch(offset);
			else if(loaded_item=='viewgroup'){
				viewGroup($('#loaded-group-id').val(), offset);
			}
			window.localStorage.setItem('offset', offset);
		}
	});
});

/*
	VIEW HOME PAGE
*/
var inc = 4;
var nextAdToShow = 0;
var ads;
function LoadHome(offset){
	window.localStorage.setItem("loaded_item", 'loadhome');
	$.ajax({
		url: getBaseUrl() + "/restCalls/application/user/home.php",
		type: "POST",
		crossDomain:true,
		global:false,
		data: {"guid": getLocalStorageItem("userid"), 'offset':offset, 'session_id':getSessionId()},
		beforeSend: function(){
			$('.user-activity div.ossn-wall-item:last').after('<div id="post-loader" style="text-align: center;font-size: 16px;color: white;background: #3278b3;">Loading..</div>');
		},
		success: function(data){
			var obj = $.parseJSON(data);
			var adsStyle = "<style> .ossn-widget .widget-heading{ color: white !important; background: #3278b3; border: 1px solid #f7f7f7 !important; } </style>";
			if(obj.html==""){
				again = false;	
			}
			if(parseInt(offset)==1){
				ads = obj.ads;
				$(".user-activity").html(obj.html);
			}
			else{
				$(".user-activity div.ossn-wall-item:last").after(obj.html);
			}
			/*Setting Ad View*/
			/*if(ads[nextAdToShow]!=null){
				$('.ossn-wall-item').eq(currentAdDiv).after(getAdsView(ads[nextAdToShow]));
			}
			else{
				nextAdToShow = 0;
				$('.ossn-wall-item').eq(currentAdDiv).after(getAdsView(ads[nextAdToShow]));
			}*/
			var i = 1;
			$('.ossn-wall-item').each(function(e) {
				if(i%inc==0){
					if(ads[nextAdToShow]!=null){
						$(this).after(getAdsView(ads[nextAdToShow]));
					}
					else{
						nextAdToShow = 0;
						$(this).after(getAdsView(ads[nextAdToShow]));
					}
					nextAdToShow++;
				}
				i++;
            });
			addOffsetClass('homePage');
			window.localStorage.setItem('offset', offset);
			//setAdsView(obj.ads);
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			$('.loader-wrapper').hide();
			$('.no-loding').css('pointer-events', 'auto')
						   .css('opacity', '1');
			busy = false;
			$('#post-loader').remove();	
			if(!again){
				$('.user-activity div.ossn-wall-item:last').after('<div id="post-loader" style="text-align: center;font-size: 16px;color: white;background: #3278b3;">No more posts</div>');
			}
		}
	});
}

function getAdsView(data){
	html="<div class='ossn-ads'> <div class='ossn-widget '> <div class='widget-heading'>MISSION</div> <div class='widget-contents'> <div class='ossn-ad-item'> <div class='row'> <!--<a class='open-with-browser' href="+data.site_url+">--> <div class='col-md-12'> <div class='ad-title'>"+data.title+"</div> <div class='ad-link'>"+data.site_url+"</div> <img class='ad-image' src='"+data.ad_url+"'> <p>"+data.description+"</p> </div> <!--</a>--> </div> </div> </div> </div> </div>";
	return html;
}

/*
	FUNTION TO HANDLE OFFSET CLICK (PAGINATION)
*/
$(document).on('click', '.pagination.ossn-pagination>li>a', function(event){
	event.preventDefault();
	var $ul = $(this).parent().parent();
	var offset = getParam($(this).attr('href'), 'offset');
	if($ul.hasClass('homePage'))
		LoadHome(offset);
	else if($ul.hasClass('userTimeLine'))
		LoadUserTimeLine(username_soc ,offset);
	else if($ul.hasClass('searchResults'))
		PerformSearch(offset);
	else if($ul.hasClass('groupTimeLine'))
		viewGroup($('#loaded-group-id').val(), offset);
	//$('html, body').animate({scrollTop:0}, 'slow');
});

/*
	TOGGLE OFFSET CLASS
*/

function addOffsetClass(className){
	$('.pagination.ossn-pagination').attr('class', 'pagination ossn-pagination');
	$('.pagination.ossn-pagination').addClass(className);
}

/*
	FUNCTION FOR HANDLING CLICK ON USER FULL NAME HREF / TIMELINE ON USER PROFILE
*/

$(document).on('click', '.owner-link, .userlink', function(event){
	event.preventDefault();
	var string = $(this).attr('href');
	var username = string.substring(string.lastIndexOf("/") + 1);
	username_soc = username;
	LoadUserTimeLine(username, 1);
	
});

function LoadUserTimeLine(username, offset){
	var gl = false;
	if(offset==1)
		gl = true;
	window.localStorage.setItem("loaded_item", 'loadusertimeline');
	$.ajax({
		url: getBaseUrl()+"restCalls/application/user/profile.php",
		type: "POST",
		data: {"username": username, 'offset':offset, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		global:gl,
		crossDomain: true,
		
		beforeSend: function(){
			$('div.ossn-wall-item:last').after('<div id="post-loader" style="text-align: center;font-size: 16px;color: white;background: #3278b3;">Loading..</div>');
		},
		success: function(data){
			if(data=="")
				again = false;
			if(offset==1)
				$('.ossn-layout-newsfeed').html(data);
			else{
				$('.ossn-layout-newsfeed').append(data);
			}
			addOffsetClass('userTimeLine');
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			$('#post-loader').remove();
			if(!again){
				$('div.ossn-wall-item:last').after('<div id="post-loader" style="text-align: center;font-size: 16px;color: white;background: #3278b3;">No more posts</div>');
			}
		}
	});
}

/*
	FUNCTION FOR VIEWING USER'S FRIEND LIST
*/

$(document).on('click', '.menu-user-timeline-friends', function(event){
	event.preventDefault();
	var guid = $(this).attr('data-guid');
	viewFriends(guid);
});

function viewFriends(guid){
	$.ajax({
		url: getBaseUrl()+"restCalls/application/user/friends.php",
		type: "POST",
		data: {"guid": guid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			//alert(data);
			$('.ossn-layout-newsfeed').html(data);
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
		}
	});
}

/*
	FUNCTION FOR VIEWING USER'S PHOTOS
*/

$(document).on('click', '.menu-user-timeline-photos', function(event){
	event.preventDefault();
	var guid = $(this).attr('data-guid');
	viewPhotos(guid)
});

function viewPhotos(guid){
	$.ajax({
		url: getBaseUrl()+"restCalls/application/user/photos.php",
		type: "POST",
		data: {"guid": guid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			//alert(data);
			$('.ossn-layout-newsfeed').html(data);
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}

/**
	FUNCTION FOR ADDING WALL POST
**/
$(document).on('submit', '#ossn-wall-form', function(e) {
	try{
		e.preventDefault();
		var $this = $(this);
		var ajaxUrl = getBaseUrl()+"restCalls/application/wall/post/user.php";
		var data = new FormData($(this)[0]);
		data.append("session_id", getLocalStorageItem('session_id'));
		data.append("ossn_cam",$("#ossn_photo").val());
		var wallowner = $(this).find("input[name='wallowner']").val();
		var isHome = false;
		if(wallowner==null || wallowner=='' || wallowner==undefined){
			ajaxUrl=getBaseUrl()+"restCalls/application/wall/post/home.php";
			isHome = true;
		}		
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data: data,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			crossDomain: true,
			global:isHome,
			
			beforeSend: function(){
				if(!isHome){
					$('.ossn-wall-post').hide();
					$('.ossn-loading').removeClass("ossn-hidden");
				}
			},
			success: function(data){
				if(data!=0){
					$('.user-activity').prepend(data);
				}
				else
					alert("Cannot create post! Pleas try again later..");
			},
			error: function(xhr, status, error){
				//alert(xhr.responseText);
			},
			complete: function(){
				if(!isHome){
					$('.ossn-loading').addClass("ossn-hidden");
					$('.ossn-wall-post').show();
				}
				$('.token-input-list').html('');
				$('#ossn-wall-friend-input-hidden').val('');
				$('#ossn-wall-friend-input-custom').val('');
				$this.find('textarea').val('');
				$('#ossn-wall-friend').hide();
				$('#file_name_text').html('No file chosen');
				$('#ossn-wall-photo').hide();
				clearInputs();
				if(isHome){
					hidePostAddPage();
				}
			}
		});
	}
	catch(e){
		alert(e.message);
	}
});

function clearInputs(){
	$('#videoThumb').remove();
	$('#thumb_hidden').remove();
	$('#thumbnailDiv').html("");
	$('#post').val('');
	$('#ossn_photo').val('');
	$('#ossn-wall-location-input').val('');
	$('#token-input-ossn-wall-friend-input').val('');
}



/*
	FUNCTION FOR VIEWING PRIVACY SELECTOR BOX
*/

$(document).on('click', '.ossn-wall-privacy', function(e) {
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/privacy.php",
		type: "POST",
		data: {'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$('.ossn-halt').addClass('ossn-light');
			$('.ossn-halt').attr('style', 'height:' + $(document).height() + 'px;');
			$('.ossn-halt').show();
			$('.ossn-message-box').html('<div class="ossn-loading ossn-box-loading"></div>');
			$('.ossn-message-box').fadeIn('slow');
		},
		success: function(data){
			$('.ossn-message-box').html(data).fadeIn();
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
});
 
/*
	SET VALUE TO HIDDEN INPUT FOR PRIVACY
*/ 
$(document).on('click', '#ossn-wall-privacy', function(e) {
	var wallprivacy = $('#ossn-wall-privacy-container').find('input[name="privacy"]:checked').val();
	$('#ossn-wall-privacy').val(wallprivacy);
	Ossn.MessageBoxClose();
});

/*
	POST EDIT VIEW FORM
*/

$(document).on('click', '.post-control-edit.ossn-wall-post-edit', function(e) {
	event.preventDefault();
	var $this = $(this);
	var pguid = $this.attr('data-guid');
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/edit.php",
		type: "POST",
		data: {'p_guid': pguid, 'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		global:false,
		
		beforeSend: function(){
			$('.ossn-halt').addClass('ossn-light');
			$('.ossn-halt').attr('style', 'height:' + $(document).height() + 'px;');
			$('.ossn-halt').show();
			$('.ossn-message-box').html('<div class="ossn-loading ossn-box-loading"></div>');
			$('.ossn-message-box').fadeIn('slow');
		},
		success: function(data){
			$('.ossn-message-box').html(data).fadeIn();
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
});

/*
	COMMENT EDIT VIEW FORM
*/
$(document).on('click', '.ossn-edit-comment', function(e){
	e.preventDefault();
	var $dataguid = $(this).attr('data-guid');
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/comment_edit.php",
		type: "POST",
		data: {'guid': $dataguid, 'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		global:false,
		
		beforeSend: function(){
			$('.ossn-halt').addClass('ossn-light');
			$('.ossn-halt').attr('style', 'height:' + $(document).height() + 'px;');
			$('.ossn-halt').show();
			$('.ossn-message-box').html('<div class="ossn-loading ossn-box-loading"></div>');
			$('.ossn-message-box').fadeIn('slow');
		},
		success: function(data){
			$('.ossn-message-box').html(data).fadeIn();
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
	
}); 

/*
	FUNCTION FOR ATTACHING COMMENT IMAGE
*/

$(document).on('change', '.ossn-comment-image-file', function(){
	var $this = $(this);
	var $container = $this.attr('data-guid');
	
	 event.preventDefault();
            var formData = new FormData($('#ossn-comment-attachment-' + $container)[0]);
			formData.append("session_id", getLocalStorageItem("session_id"));
            $.ajax({
                url: getBaseUrl() + 'restCalls/application/wall/comment_attachment.php',
                type: 'POST',
                data: formData,
                async: true,
				global:false,
                beforeSend: function() {
                    $('#ossn-comment-attachment-' + $container).find('.image-data')
                        .html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" style="width:30px;border:none;" />');
                    $('#comment-attachment-container-' + $container).show();

                },
                cache: false,
                contentType: false,
                processData: false,
                success: function(callback) {
					//console.log(callback);
                    if (callback['type'] == 1) {
                        $('#comment-container-' + $container).find('input[name="comment-attachment"]').val(callback['file']);
                        $('#ossn-comment-attachment-' + $container).find('.image-data')
                            .html('<img src="' + Ossn.site_url + 'comment/staticimage?image=' + callback['file'] + '" />');
                    }
                    if (callback['type'] == 0) {
                        $('#comment-container-' + $container).find('input[name="comment-attachment"]').val('');
                        $('#comment-attachment-container-' + $container).hide();
                        Ossn.MessageBox('syserror/unknown');
                    }

                },
            });
});
 

/*$(document).on('submit', '.comment-box', function(e) {
	try{
		e.preventDefault();
		alert("caight");
		return;
		var data = new FormData($(this)[0]);
		$.ajax({
			url: getBaseUrl()+"restCalls/application/wall/post/home.php",
			type: "POST",
			data: data,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			crossDomain: true,
			
			beforeSend: function(){
				$('#wall-post-submit').hide();
				$('.ossn-loading').removeClass("ossn-hidden");
			},
			success: function(data){
				console.log(data);
				data = $.parseJSON(data);
				console.log(data.test);
				if(!data.hasOwnProperty("errmessage")){
					//alert(console.log(data.template));
					$('.user-activity').prepend(data.template);
				}
				else
					alert(data.errmessage);
			},
			error: function(xhr, status, error){
				alert(xhr.responseText);
			},
			complete: function(){
				$('.ossn-loading').addClass("ossn-hidden");
				$('#wall-post-submit').show();
				$('#post').val('');
			}
		});
	}
	catch(e){
		alert(e.message);
	}
});*/

/*
	FUNCTION FOR DELETING WALL POST
*/

 $(document).on('click', '.ossn-wall-post-delete', function(e) {
	$url = $(this);
	e.preventDefault();
	$.ajax({
			url: getBaseUrl() + "restCalls/application/wall/post/delete.php", //$url.attr('href')+"&session_id="+getLocalStorageItem('session_id'),
			type: "POST",
			data:{"post": $url.attr('data-guid'), "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			global:false,
			
			beforeSend: function(){
				$('#activity-item-' + $url.attr('data-guid')).attr('style', 'opacity:0.5;');
			},
			success: function(callback){
				//alert(callback);
				if (callback == 1) {
					$('#activity-item-' + $url.attr('data-guid')).fadeOut();
					$('#activity-item-' + $url.attr('data-guid')).remove();
				} else {
					$('#activity-item-' + $url.attr('data-guid')).attr('style', 'opacity:1;');
				}
			},
			error: function(xhr, status, error){
				//alert("error:"+xhr.responseText);
			},
			complete: function(){
				
			}
	});
});


/***** FUNCTION FOR EDITING POST AND COMMENT, ADDING NEW ALBUM AND NEW PHOTO ******/

$(document).on('submit', '.ossn-form:not("#ossn-wall-form, #post-in-group, .comment-container")', function(e) {
	//if($(this).parent().parent().hasClass('ossn-box-inner')){
		e.preventDefault();
		var $this = $(this);
		var data = new FormData($(this)[0]);
		var guid = $(this).parent().find(('input[name=guid]')).val();
		var url = getBaseUrl() + $(this).attr('data-action');
		data.append("session_id" , getLocalStorageItem('session_id'));
		
		$.ajax({
			url: url ,
			type: "POST",
			data:data,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			crossDomain: true,
			global:false,
			
			beforeSend: function(){
				disableClicks($this.find("input[type='submit']"));
			},
			success: function(data){
				console.log(data);
				var obj = $.parseJSON(data);
				//alert("data:"+obj.post);
				
				if(!obj.hasOwnProperty('err') && obj.type=='wall')
					$('#activity-item-'+guid).find('.post-contents').find('p').html(obj.text);
				else if(!obj.hasOwnProperty('err') && obj.type=='comment'){
					var text = obj.text;
					$('#comments-item-'+guid).find('.comment-contents').find('p').html(text);
				}
				else if(!obj.hasOwnProperty('err') && obj.type=='album_add')
					$('.row.ossn-profile-bottom').html(obj.text);
				else if(!obj.hasOwnProperty('err') && obj.type=='photo')
					$('.ossn-profile.container').html(obj.text);
				else if(!obj.hasOwnProperty('err') && obj.type=='profileSave'){
					if(obj.hasOwnProperty('err-pro')){
						alert(obj.errpro);
					}
					else{
						$('.row.ossn-profile-bottom').html(obj.text);
						 $('html, body').animate({scrollTop:0}, 'slow');
					}
					
				}
				else if(!obj.hasOwnProperty('err') && obj.type=='groupAdd'){
					//alert(obj.text);
					window.parent.groupAdd(obj);
				}
				else if(!obj.hasOwnProperty('err') && obj.type=='groupPost'){
					$('.user-activity').prepend(obj.text);
					
				}
				else if(!obj.hasOwnProperty('err') && obj.type=='editGroup'){
					$('.ossn-layout-newsfeed').html(obj.text);
				}
				else if(obj.hasOwnProperty('err'))
					console.log(obj.err);
				Ossn.MessageBoxClose();
			},
			error: function(xhr, status, error){
				//alert("error:"+xhr.responseText);
			},
			complete: function(){
				enableClicks($this.find("input[type='submit']"));
			}
		});
	//}
});

/*
	FUNCTION FOR ADDING POST IN GROUP
*/

$(document).on('submit', '#post-in-group', function(e) {
	//if($(this).parent().parent().hasClass('ossn-box-inner')){
		e.preventDefault();
		var $this = $(this);
		var data = new FormData($(this)[0]);
		var guid = $(this).parent().find(('input[name=guid]')).val();
		var url = getBaseUrl() + $(this).attr('data-action');
		data.append("session_id" , getLocalStorageItem('session_id'));
		
		$.ajax({
			url: url ,
			type: "POST",
			data:data,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			crossDomain: true,
			
			beforeSend: function(){
				disableClicks($this.find("input[type='submit']"));
			},
			success: function(data){
				if(data!='0' && data!=''){
					$('.user-activity').prepend(data);
					$('.tabs-input').find("textarea").val('');
					$('#ossn-wall-photo').find("input[name=ossn_photo]").val('');
					clearInputs();
				}
			},
			error: function(xhr, status, error){
				//alert("error:"+xhr.responseText);
			},
			complete: function(){
				enableClicks($this.find("input[type='submit']"));
			}
		});
	//}
});


/**
	FUNTION FOR ADDING COMMENT
**/
$(document).on('submit', '.ossn-form.comment-container', function(event) {
	event.preventDefault();
	var data = new FormData($(this)[0]);
	$container = $(this).find("input[name=post]").val();
	is_entity=0;
	if($container==null || $container==undefined || $container==''){
		$container = $(this).find("input[name=entity]").val();
		is_entity = 1;
	}
	data.append("session_id" , getLocalStorageItem('session_id'));
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/post/comment.php",
		type: "POST",
		data:data,
		async: true,
		cache: false,
		contentType: false,
		processData: false,
		global:false,
		crossDomain: true,
		
		beforeSend: function(){
			$('#comment-box-' + $container).attr('readonly', 'readonly');
		},
		success: function(data){
			callback = data;
			if (callback['process'] == 1) {
				if($('#comment-container').is(":visible")){
					$('#comment-container').find('#comment-box-' + $container).removeAttr('readonly');
					$('#comment-container').find('#comment-box-' + $container).val('');
					$('#comment-container').find('.ossn-comments-list-' + $container).prepend(callback['comment']);
					$('#comment-container').find('#comment-attachment-container-' + $container).hide();
					$('#comment-container').find('#ossn-comment-attachment-' + $container).find('.image-data').html('');
					//commenting pic followed by text gives warnings #664 $dev.githubertus
					$('#comment-container').find('#comment-container-' + $container).find('input[name="comment-attachment"]').val('');
					sendCommentNotification($container, is_entity);
				}
				else{
					$('#comment-box-' + $container).removeAttr('readonly');
					$('#comment-box-' + $container).val('');
					$('.ossn-comments-list-' + $container).prepend(callback['comment']);
					$('#comment-attachment-container-' + $container).hide();
					$('#ossn-comment-attachment-' + $container).find('.image-data').html('');
					//commenting pic followed by text gives warnings #664 $dev.githubertus
					$('#comment-container-' + $container).find('input[name="comment-attachment"]').val('');
				}
			}
			if (callback['process'] == 0) {
				if($('#comment-container').is(":visible")){
					$('#comment-container').find('#comment-box-' + $container).removeAttr('readonly');
					Ossn.MessageBox('syserror/unknown');
				}
				else{
					$('#comment-box-' + $container).removeAttr('readonly');
					Ossn.MessageBox('syserror/unknown');
				}
			}
				
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
			//$('#activity-item-'+)
		},
		complete: function(){
			
		}
	});
});
/****** FUNCTIN FOR DELETEING COMMENT ****/

$(document).on('click', '.ossn-delete-comment', function(event){
	event.preventDefault();
	var url = $(this).attr('href');
	$comment_id = getParam(url, 'comment');
	
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/post/comment_delete.php",
		type: "POST",
		data:{"comment": $comment_id, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		global:false,
		
		beforeSend: function(){
			if($('#comment-container').is(":visible")){
				$('#comment-container').find('#comments-item-' + $comment_id).attr('style', 'opacity:0.6;');
			}
			else{
				$('#comments-item-' + $comment_id).attr('style', 'opacity:0.6;');
			}
		},
		success: function(data){
			callback = data;
			if (callback == 1) {
				if($('#comment-container').is(":visible")){
					$('#comment-container').find('#comments-item-' + $comment_id).fadeOut().remove();
				}
				else{
					$('#comments-item-' + $comment_id).fadeOut().remove();
				}
			}
			if (callback == 0) {
				if($('#comment-container').is(":visible")){
					$('#comment-container').find('#comments-item-' + $comment_id).attr('style', 'opacity:0.6;');
				}
				else{
					$('#comments-item-' + $comment_id).attr('style', 'opacity:0.6;');
				}
			}
				
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
			//$('#activity-item-'+)
		},
		complete: function(){
			
		}
	});
});

/*
	FUNCTON FOR LIKEING WALL POST
*/
function OssnPostLike(post){
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/post/Like/like.php",
		type: "POST",
		data:{"post": post, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		global:false,
		
		beforeSend: function(){
			 $('#ossn-like-' + post).html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
			 $('#starpoplabel'+post).find('a').html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
            if (callback['done'] !== 0) {
                $('#ossn-like-' + post).html(callback['button']);
                $('#ossn-like-' + post).attr('onClick', 'OssnPostUnlike(' + post + ');');
				$('#starpoplabel'+post).find('a').html('<b>Star</b>');
				if($("#activity-item-"+post+" .comments-likes .like-share").length==0)
					$("#activity-item-"+post+" .comments-likes").prepend('<div class="like-share"><i class="fa fa-star"></i>You starry this</div>');
				else{
					$("#activity-item-"+post+" .comments-likes").find('.like-share').html('<i class="fa fa-star"></i>You and <a onclick="Ossn.ViewLikes('+post+');" href="#">'+$('#activity-item-'+post+' .comments-likes .like-share a').html()+'</a> starry this');
					sendLikeNotification(post, 0);
				}
            } else {
                $('#ossn-like-' + post).html(Ossn.Print('<i class="fa fa-star-o" aria-hidden="true"></i>Star'));
				$('#starpoplabel'+post).find('a').html('Star');
            }
			
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			window.setTimeout(function (){$('#starpoplabel'+post).parent().hide(); }, 500);
		}
	});
	
}

/*
	FUNCTION FOR UNLIKING WALL POST
*/

function OssnPostUnlike(post){
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/post/Like/unlike.php",
		type: "POST",
		data:{"post": post, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		global:false,
		
		beforeSend: function(){
			 $('#ossn-like-' + post).html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
			 $('#starpoplabel'+post).find('a').html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
            if (callback['done'] !== 0) {
                $('#ossn-like-' + post).html(callback['button']);
                $('#ossn-like-' + post).attr('onclick', 'OssnPostLike(' + post + ');');
				$('#starpoplabel'+post).find('a').html('Star');
				
				if($("#activity-item-"+post+" .comments-likes .like-share a").length==0)
					$("#activity-item-"+post+" .comments-likes").find('.like-share').remove();
				else{
					$("#activity-item-"+post+" .comments-likes").find('.like-share').html('<i class="fa fa-star"></i><a onclick="Ossn.ViewLikes('+post+');" href="#">'+$('#activity-item-'+post+' .comments-likes .like-share a').html()+'</a> starry this');
				}
				
            } else {
                $('#ossn-like-' + post).html(Ossn.Print('unlike'));
				$('#starpoplabel'+post).find('a').html('<b>Star</b>');
            }
			
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			window.setTimeout(function (){$('#starpoplabel'+post).parent().hide(); }, 1500);
		}
	});
}
/*
	FUNCTION FOR LIKING ENTITY
*/

OssnEntityLike = function(postid) {
	$.ajax({
		url: getBaseUrl() + 'restCalls/application/wall/post/Like/like.php',
		type: "POST",
		data:{"entity": postid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		global:false,
		
		beforeSend: function(){
			$("#ossn-like-" + postid).html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
			console.log(callback);
            if (callback['done'] !== 0) {
                $("#ossn-like-" + postid).html(callback['button']);
                $("#ossn-like-" + postid).attr('onClick', 'OssnEntityUnlike(' + postid + ');');
				if($("#activity-item-"+post+" .comments-likes .like-share").length==0)
					$("#activity-item-"+post+" .comments-likes").prepend('<div class="like-share"><i class="fa fa-star"></i>You starry this</div>');
				else{
					$("#activity-item-"+post+" .comments-likes").find('.like-share').html('<i class="fa fa-star"></i>You and <a onclick="Ossn.ViewLikes('+post+');" href="#">'+$('#activity-item-'+post+' .comments-likes .like-share a').html()+'</a> starry this');
				}
            } else {
                $("#ossn-like-" + postid).html(Ossn.Print('like'));
            }
			
		},
		error: function(xhr, status, error){
			$("#ossn-like-" + postid).html(Ossn.Print('like'));
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
};

/*
	FUNCTION FOR UNLIKING ENTITY
*/

OssnEntityUnlike = function(postid) {
	console.log("entity unliking");
	$.ajax({
		url: getBaseUrl() + 'restCalls/application/wall/post/Like/unlike.php',
		type: "POST",
		data:{"entity": postid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		global:false,
		
		beforeSend: function(){
			$('#ossn-like-' + postid).html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
			 if (callback['done'] !== 0) {
                $('#ossn-like-' + postid).html(callback['button']);
                $('#ossn-like-' + postid).attr('onclick', 'OssnEntityLike(' + postid + ');');
				
            } else {
                $('#ossn-like-' + postid).html(Ossn.Print('unlike'));
            }
			
		},
		error: function(xhr, status, error){
			$("#ossn-like-" + postid).html(Ossn.Print('Unlike'));
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
};

/*
	FUNCTION FOR LIKING/UNLIKING COMMENT
*/
$(document).on('click', '.ossn-like-comment', function(event){
	event.preventDefault();
	$item = $(this);
	var ajaxUrl = getBaseUrl() + "restCalls/application/wall/post/Like/annotation_like.php";
	$annotation = getParam($item.attr('href'),'annotation');
	var $type = $.trim($item.attr('data-type'));
	if($type == 'Unlike')
		ajaxUrl = getBaseUrl() + "restCalls/application/wall/post/Like/annotation_unlike.php"
    var $url = $item.attr('href');
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"annotation": $annotation, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		global:false,
		
		beforeSend: function(){
			$item.html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
			
			if (callback['done'] == 1) {
				$total_guid = Ossn.UrlParams('annotation', $url);
				$total = $('.ossn-total-likes-' + $total_guid).attr('data-likes');
				if ($type == 'Like') {
					$item.html('<b style="color:#337ab7"><i class="fa fa-hand-o-right"aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></b>');
					$item.attr('data-type', 'Unlike');                            
					var unlike = $url.replace("like", "unlike");
					$item.attr('href', unlike);
					$total_likes = $total;
					$total_likes++;
					$('.ossn-total-likes-' + $total_guid).attr('data-likes', $total_likes);
					$('.ossn-total-likes-' + $total_guid).html('<i class="fa fa-thumbs-up"></i>' + $total_likes);
				}
				if ($type == 'Unlike') {
					$item.html('<b style="color:black"><i class="fa fa-hand-o-right"aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></b>');
					$item.attr('data-type', 'Like');                            
					var like = $url.replace("unlike", "like");
					$item.attr('href', like);
					if ($total > 1) {
						$like_remove = $total;
						0
						$like_remove--;
						$('.ossn-total-likes-' + $total_guid).attr('data-likes', $like_remove);
						$('.ossn-total-likes-' + $total_guid).html('<i class="fa fa-thumbs-up"></i>' + $like_remove);
					}
					if ($total == 1) {
						$('.ossn-total-likes-' + $total_guid).attr('data-likes', 0);
						$('.ossn-total-likes-' + $total_guid).html('');
	
					}
				}
			}
			if (callback['done'] == 0) {
				if ($type == 'Like') {
					$item.html(Ossn.Print('like'));
					$item.attr('data-type', 'Like');
					Ossn.MessageBox('syserror/unknown');
				}
				if ($type == 'Unlike') {
					$item.html(Ossn.Print('unlike'));
					$item.attr('data-type', 'Unlike');
					Ossn.MessageBox('syserror/unknown');
	
				}
			}
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
			//$('#activity-item-'+)
		},
		complete: function(){
			
		}
	});
});

/*
	FUNCTION FOR ADD/REMOVE FRIEND
*/

$(document).on('click', '.friend-add-a', function(event){
	event.preventDefault();
	var $this = $(this);
	var ajaxUrl = getBaseUrl() + "restCalls/application/friend/add.php";
	if($this.attr('data-action')=='remove')
		ajaxUrl = getBaseUrl() + "restCalls/application/friend/remove.php";
	var add_guid = getParam($this.attr('href'), 'user');
	$this.addClass('disableClick');
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"user": add_guid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(callback){
			//alert(callback);
			var obj = $.parseJSON(callback);
			if(obj.type==1){
				if($this.attr('data-action')=='add'){
					if($this.hasClass('btn-primary')){
						$this.removeClass('btn-primary')
							 .addClass('btn-danger')
							 .html('Unfriend')
							 .attr('data-action', 'remove');
					}
					else
						$this.html('Unfriend')
							 .attr('data-action', 'remove');
					sendFriendRequestNotification(add_guid, 1);
				}
				else{
					if($this.hasClass('btn-danger')){
						$this.removeClass('btn-danger')
							 .addClass('btn-primary')
							 .html('Add friend')
							 .attr('data-action', 'add');
					}
					else
						$this.html('Add friend')
							 .attr('data-action', 'add');
				}
			}
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			$this.removeClass('disableClick');
		}
	});
});

/*
	FUNCTION TO UPLOAD USER COVER PHOTOS
*/

$(document).on('change', '#upload-cover>.coverfile', function(event){
	$('#upload-cover').find("input[type=submit]").click();
});

$(document).on('submit', '#upload-cover', function(event){
	event.preventDefault();
	var formData = new FormData($(this)[0]);
	formData.append('session_id', getLocalStorageItem('session_id'));
	var $url = getBaseUrl() + 'restCalls/application/user/cover/upload.php';
	$.ajax({
		url: $url,
		type: 'POST',
		data: formData,
		async: true,
		cache: false,
		contentType: false,
		processData: false,
		beforeSend: function(xhr, obj) {
			$('.profile-cover-img').attr('class', 'user-cover-uploading');

			var fileInput = $('#upload-cover').find("input[type=file]")[0],
				file = fileInput.files && fileInput.files[0];

			if (file) {
				var img = new Image();

				img.src = window.URL.createObjectURL(file);

				img.onload = function() {
					var width = img.naturalWidth,
						height = img.naturalHeight;

					window.URL.revokeObjectURL(img.src);
					if (width < 850 || height < 300) {
						xhr.abort();
						alert(Ossn.Print('profile:cover:err1:detail'), 'error');
						return false;
					}
				};
			}
		},
		success: function(callback) {
			console.log(callback);
			$time = $.now();
			$('.profile-cover').find('img').removeClass('user-cover-uploading');
			$imageurl = $('.profile-cover').find('img').attr('src') + '?' + $time;
			$('.profile-cover').find('img').attr('src', $imageurl);
			$('.profile-cover').find('img').attr('style', '');
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});	
});

/*
	FUNCTION TO UPLOAD USER PROFILE PHOTOS
*/

$(document).on('change', '#upload-photo>.pfile', function(event){
	$('#upload-photo').find("input[type=submit]").click();
});

$(document).on('submit', '#upload-photo', function(event){
	event.preventDefault();
	var formData = new FormData($(this)[0]);
	formData.append('session_id', getLocalStorageItem('session_id'));
	var $url = getBaseUrl() + 'restCalls/application/user/profile/upload.php';
	$.ajax({
		url: $url,
		type: 'POST',
		data: formData,
		async: true,
		cache: false,
		contentType: false,
		processData: false,
		beforeSend: function() {
			$('.upload-photo').attr('class', 'user-photo-uploading');
		},
		success: function(callback) {
			$time = $.now();
			$('.user-photo-uploading').attr('class', 'upload-photo').hide();
			$imageurl = $('.profile-photo').find('img').attr('src') + '?' + $time;
			$('.profile-photo').find('img').attr('src', $imageurl);
			$topbar_icon_url = $('.ossn-topbar-menu').find('img').attr('src') + '?' + $time;
			$('.ossn-topbar-menu').find('img').attr('src', $topbar_icon_url);
			$('.upload-photo').show();
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});	
});



/*
	FUNCTION FOR POKING/BlOCKING/UNBLOCKING USER
*/
$(document).on('click', '.profile-menu-extra-poke, .profile-menu-extra-block', function(event){
	event.preventDefault();
	var $this = $(this);
	var ajaxUrl ='';
	var guid = getParam($this.attr('href'), 'user');
	$this.addClass('disableClick');
	
	//Poke
	if($this.hasClass('profile-menu-extra-poke')){
		ajaxUrl = getBaseUrl() + "restCalls/application/user/poke.php";
	}
	//Block//UnBlock
	else if($this.hasClass('profile-menu-extra-block')){
		if($this.html()=='Block')
			ajaxUrl = getBaseUrl() + "restCalls/application/user/block.php";
		else
			ajaxUrl = getBaseUrl() + "restCalls/application/user/unblock.php";
	}
	if(ajaxUrl!=''){
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data:{"user": guid, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				
			},
			success: function(callback){
				//alert(callback);
				var obj = $.parseJSON(callback);
				if(obj.done==1){
					if($this.html()=='Poke'){
						/*
							Handle Poke Success
						*/
						alert("Success");
					}
					else if($this.text()=='Block'){
						$this.text('Unblock');
					}
					else if($this.text()=='Unblock'){
						$this.text('Block');
					}
				}
			},
			error: function(xhr, status, error){
				//alert("error:"+xhr.responseText);
			},
			complete: function(){
				$this.removeClass('disableClick');
			}
		});
	}
});

/*
	FUNCTION FOR VIEWING USER PHOTOS -- HANDLES CLICK ON ALBUM
*/

var local_guid;
var local_type;

$(document).on('click', '.album-thumbnail', function(event){
	event.preventDefault();
	var $this = $(this);
	var guid = $this.attr('data-guid');
	local_guid = guid;
	var type = $this.attr('data-type');
	local_type = type;
	LoadAlbum(guid, type);
});

function LoadAlbum(guid, type){
	var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/album/view_photos.php"
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"guid": guid, "type": type, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(callback){
			$('.ossn-profile.container').html(callback).append("<input type='hidden' value='"+guid+"' id='album-guid'>");
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
}

/*
	FUNCTION FOR VIEWING PHOTO IN AN ANLBUM - HANDLES CLICK ON INDIVIDUAL PHOTO
*/
$(document).on('click', '.pthumb.image-view', function(event){
	event.preventDefault();
	var $this = $(this).parent();
	var guid = $this.attr('data-guid');
	var type = $this.attr('data-type');
	loadPhoto(guid, type, 'self', false);
});

function loadPhoto(guid, type, from, isInsp){
	var g = true;
	if(isInsp)
		g = false;
	var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/photo/view_photo.php"
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"guid": guid, "type": type, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		global:g,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(callback){
			if(!isInsp){
				if(from=='self')
					$('.ossn-profile.container').html(callback);
				else{
					$('.ossn-layout-newsfeed').html(callback);
					$('.back-to-album').hide();
				}
			}
			else{
				$('.ossn-layout-newsfeed').hide();
				$('#comment-container').html(callback)
									   .show();
				$('#comment-container').find('.comments-list').show();
				$('#comment-container').find('#comment-box-'+guid).focus();
				
				
				$('#comment-container').find('.page-sidebar').hide();
				$('#comment-container').find('.back-to-album').hide();
				$('#comment-container').find('.comments-list').show();
				$('#comment-container').find('.ossn-photo-view-controls').hide();
				$('#comment-container').find('#comment-box-'+guid).focus();
			}
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
}

/*
	FUNCTION TO GO BACK TO ALBUM VIEW
*/
$(document).on('click', '.button-grey.back-button', function(event){
	event.preventDefault();
	if(username_soc!='' && username_soc!=undefined && username_soc!=null)
		$.ajax({
			url: getBaseUrl() + "restCalls/application/user/photo/album/back_to_album.php",
			type: "POST",
			data:{"username":username_soc, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				
			},
			success: function(callback){
				$('.ossn-layout-newsfeed').html(callback);
			},
			error: function(xhr, status, error){
				//alert("error:"+xhr.responseText);
			},
			complete: function(){
				
			}
		});
	else{
		location.reload();
	}
	
});

/*
	FUNCTION TO GO BACK TO PHOTOS VIEW - INSIDE AN ALBUM
*/

$(document).on('click', '.back-to-album', function(event){
	event.preventDefault();
	var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/album/view_photos.php";
	if(local_guid!='' && local_guid!=null && local_guid!=undefined && local_type!='' && local_type!=null && local_type!=undefined){
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data:{"guid": local_guid, "type": local_type, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				
			},
			success: function(callback){
				$('.ossn-profile.container').html(callback);
			},
			error: function(xhr, status, error){
				//alert("error:"+xhr.responseText);
			},
			complete: function(){
				
			}
		});
	}
	else
		location.reload();
});

/*
	FUNCTION FOR VIEWING PHOTO ADD DIALOG BOX
*/

$(document).on('click', '#ossn-add-photos', function(e){
	var $dataurl = local_guid; //$('#album-guid').val();
	var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/photos/add_form.php";
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"album": $dataurl, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$('.ossn-halt').addClass('ossn-light');
			$('.ossn-halt').attr('style', 'height:' + $(document).height() + 'px;');
			$('.ossn-halt').show();
			$('.ossn-message-box').html('<div class="ossn-loading ossn-box-loading"></div>');
			$('.ossn-message-box').fadeIn('slow');
		},
		success: function(callback){
			$('.ossn-message-box').html(callback).fadeIn();
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
}); 

/*
	FUNCTION FOR DELETING ALBUM
*/

$(document).on('click', '.button-grey.delete-album', function(event){
	event.preventDefault();
	if(confirmOperation()){
		var $this = $(this);
		var guid = $this.attr('data-guid');
		var username = username_soc;
		var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/album/delete.php";
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data:{"guid": guid, "username": username, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				disableClicks($this);
			},
			success: function(callback){
				$('.ossn-layout-newsfeed').html(callback);
			},
			error: function(xhr, status, error){
				//alert("error:"+xhr.responseText);
			},
			complete: function(){
				enableClicks($this);
			}
		});
	}
}); 

/*
	FUNCTION FOR DELETING A PHOTO
*/

$(document).on('click', '.btn-danger.delete-photo', function(event){
	event.preventDefault();
	if(confirmOperation()){
		var $this = $(this);
		var album_type = 'view';
		if($this.hasClass('profile'))
			album_type = 'profile';
		else if($this.hasClass('cover'))
			album_type = 'cover';
		var id = getParam($this.attr('href'), 'id');
		var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/photo/delete.php";
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data:{"id": id, 'album_type':album_type, "guid": local_guid, "type":local_type, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				disableClicks($this);
			},
			success: function(callback){
				$('.ossn-profile.container').html(callback);
			},
			error: function(xhr, status, error){
				//alert("error:"+xhr.responseText);
			},
			complete: function(){
				enableClicks($this);
			}
		});
	}
});

/*
	FUNCTION FOR UPDATING USER INFO - HANDLES CLICK ON UPDATE INFO BUTTON IN USER TIMELINE
*/

$(document).on('click', '.btn-action.update-user-info', function(event){
	event.preventDefault();
	var $this = $(this);
	var guid = $this.attr('data-guid');
	LoadEditUserPage(guid); 	
});

function LoadEditUserPage(guid){
	$.ajax({
		url: getBaseUrl()+"restCalls/application/user/profile_edit.php",
		type: "POST",
		data:{"guid": guid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(callback){
			$('.ossn-layout-newsfeed').html(callback);
		},
		error: function(xhr, status, error){
			//alert("error:"+xhr.responseText);
		},
		complete: function(){
		}
	});
}

/*
	FUNCTION TO VIEW GROUP
*/

function viewGroup(guid, offset){
	console.log(offset);
	window.localStorage.setItem("loaded_item", 'viewgroup');
	var gl = false;
	if(parseInt(window.localStorage.getItem('offset'))==1)
		var gl = true;
	event.preventDefault();
	$.ajax({
		url: getBaseUrl() + "/restCalls/application/group/actions/view.php",
		type: "POST",
		data: {'guid':guid, 'offset': offset, 'session_id': window.localStorage.getItem('session_id')},
		async: true,
		cache: false,
		global:gl,
		crossDomain: true,
		
		beforeSend: function(){
			$('.user-activity div.ossn-wall-item:last').after('<div id="post-loader" style="text-align: center;font-size: 16px;color: white;background: #3278b3;">Loading..</div>');
		},
		success: function(data){
			//console.log(data);
			if(data=="")
				again = false;
			if(parseInt(offset)==1)
				$('.ossn-layout-newsfeed').html(data);
			else
				$('.ossn-layout-newsfeed div.ossn-wall-item:last').after(data);
			if($('#loaded-group-id').length)
				$('#loaded-group-id').val(guid);
			else
				$('.ossn-layout-newsfeed').append('<input type="hidden" id="loaded-group-id" value="'+guid+'">');
			addOffsetClass('groupTimeLine');
			
			
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			busy = false;
			$('#post-loader').remove();	
			if(!again){
				$('.user-activity div.ossn-wall-item:last').after('<div id="post-loader" style="text-align: center;font-size: 16px;color: white;background: #3278b3;">No more posts</div>');
			}
		}
	});
}

/*
	FUNCTION TO VIEW ADD GROUP FORM
*/

$(document).on('click', '#ossn-group-add', function() {
	event.preventDefault();
	var $this = $(this);
	$.ajax({
		url: getBaseUrl() + "restCalls/application/group/add_group_form.php",
		type: "POST",
		data: {'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$('.ossn-halt').addClass('ossn-light');
			$('.ossn-halt').attr('style', 'height:' + $(document).height() + 'px;');
			$('.ossn-halt').show();
			$('.ossn-message-box').html('<div class="ossn-loading ossn-box-loading"></div>');
			$('.ossn-message-box').fadeIn('slow');
		},
		success: function(data){
			$('.ossn-message-box').html(data).fadeIn();
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
});

/*
	FUNCTION TO VIEW GROUP JOIN REQUESTS
*/

$(document).on('click', '#group-view-requests', function() {
	event.preventDefault();
	var $this = $(this);
	var subpage = $this.attr('data-type');
	var guid = $this.attr('data-guid');
	viewSubpages(subpage, guid);
});

/*
	VIEW GROUP MEMBERS
*/
$(document).on('click', '.menu-group-timeline-members', function() {
	event.preventDefault();
	var subpage = 'members';
	var guid =	$('#loaded-group-id').val();
	viewSubpages(subpage, guid);
});

/*
	VIEW SETTINGS PAGE
*/
$(document).on('click', '.group-settings', function() {
	event.preventDefault();
	var subpage = 'edit';
	var guid =	$('#loaded-group-id').val();
	viewSubpages(subpage, guid);
});

/*
	FUNCTION TO VIEW SUBPAGES OF GROUP
*/

function viewSubpages(subpage, guid){
	$.ajax({
		url: getBaseUrl() + "restCalls/application/group/subpage.php",
		type: "POST",
		data: {'subpage': subpage, 'guid':guid, 'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			$('.ossn-layout-newsfeed').html(data);
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}

/*
	FUNCTION TO APPROVE/DECLINE REQUEST TO JOIN GROUP
*/

$(document).on('click', '.right.request-controls>a', function() {
	event.preventDefault();
	var $this = $(this);
	var op =	$this.attr('href');
	var operation = $this.attr('data-type');
	var group_guid = getParam(op, 'group');
	var user_guid = getParam(op, 'user');
	$.ajax({
		url: getBaseUrl() + "restCalls/application/group/actions/member/request/"+operation+".php",
		type: "POST",
		data: {'gGuid':group_guid, 'uGuid':user_guid, 'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			console.log(data);
			if(data==1){
				$this.parent().parent().parent().parent().hide();
				
			}
			else{
				//alert('Error');
			}
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
});

/*
	FUNCTION TO UPLOAD GROUP COVER PHOTO
*/

$(document).on('change', '#group-upload-cover>.coverfile', function(){
	$("#group-upload-cover").find('.upload').click();
});

$(document).on('submit', '#group-upload-cover', function(event) {
	event.preventDefault();
	var formData = new FormData($(this)[0]);
	formData.append('session_id', getLocalStorageItem('session_id'));
	var $url = getBaseUrl() + 'restCalls/application/group/actions/cover/upload.php';
	$.ajax({
		url: $url,
		type: 'POST',
		data: formData,
		async: true,
		beforeSend: function(xhr, obj) {
			if ($('.ossn-group-cover').length == 0) {
				$('.header-users').attr('style', 'opacity:0.7;');
			} else {
				$('.ossn-group-cover').attr('style', 'opacity:0.7;');
			}
			var fileInput = $('#group-upload-cover').find("input[type='file']")[0],
				file = fileInput.files && fileInput.files[0];

			if (file) {
				var img = new Image();

				img.src = window.URL.createObjectURL(file);

				img.onload = function() {
					var width = img.naturalWidth,
						height = img.naturalHeight;

					window.URL.revokeObjectURL(img.src);
					if (width < 850 || height < 300) {
						xhr.abort();
						alert(Ossn.Print('profile:cover:err1:detail'), 'error');
						return false;
					}
				};
			}
		},
		cache: false,
		contentType: false,
		processData: false,
		success: function(callback) {
			console.log("callback:"+callback);
			if (callback['type'] == 1) {
				if ($('.ossn-group-cover').length == 0) {
					viewGroup($('#loaded-group-id').val(), 1);
				} else {
					$('.ossn-group-cover').attr('style', '');
					$('.ossn-group-cover').find('img').attr('style', '');
					$('.ossn-group-cover').find('img').attr('src', callback['url']);
				}
			}
			if (callback['type'] == 0) {
				Ossn.MessageBox('syserror/unknown');
			}
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		}
	});
	return false;
});

/*
	FUNCTION TO DELETE A GROUP
*/

$(document).on('click', '.delete-group', function(event){
	event.preventDefault();
	if(confirmOperation()){
		var guid = getParam($(this).attr('href'), 'guid');
		$.ajax({
			url: getBaseUrl() + "restCalls/application/group/actions/delete.php",
			type: "POST",
			data: {'guid': guid, 'session_id': getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				
			},
			success: function(data){
				if(data!='0'){
					window.parent.groupDelete(guid);
					$('.ossn-layout-newsfeed').html(data);
					addOffsetClass('homePage');
				}
				else
					alert('error');
			},
			error: function(xhr, status, error){
				//alert(xhr.responseText);
			},
			complete: function(){
				
			}
		});
	}
});

/*
	FUNCTION FOR JOINING A GROUP
*/

$(document).on('click', '.group-join, .group-join-cancel', function(event){
	event.preventDefault();
	var $this = $(this);
	var operation = $this.attr('data-ops');
	var guid = getParam($this.attr('href'), 'group');
	var ajaxUrl = '';
	if(operation=='join')
		ajaxUrl = getBaseUrl() + "restCalls/application/group/actions/join.php"
	else if(operation=='cancel')
		ajaxUrl = getBaseUrl() + "restCalls/application/group/actions/member/request/cancel.php";
	if(ajaxUrl!=''){
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data: {'operation': operation, 'group': guid, 'session_id': getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				
			},
			success: function(data){
				if(data==1){
					if(operation=='join'){
						$this.attr('data-ops', 'cancel');
						$this.text('Membership cancel');
					}
					else{
						$this.attr('data-ops', 'join');
						$this.text('Join Group');
					}
				}
			},
			error: function(xhr, status, error){
				//alert(xhr.responseText);
			},
			complete: function(){
				
			}
		});
	}
});

/*
	FUNCTION FOR SEARCH
*/

$(document).on('click', '.ossn-menu-search-users, .ossn-menu-search-groups', function(event){
	event.preventDefault();
	var $this = $(this);
	var type = getParam($this.find('a').attr('href'), 'type');
	$('#searched-type').val(type);
	PerformSearch(1);
});

function PerformSearch(offset){
	window.localStorage.setItem("loaded_item", 'performsearch');
	$.ajax({
		url: getBaseUrl() + "restCalls/application/search.php",
		type: "POST",
		data: {'q':$('#searched-query').val(), 'offset': offset, 'type':$('#searched-type').val(), 'session_id': window.localStorage.getItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			//console.log(data);
			if($('#searched-type').val()=='users')
				$('.ossn-layout-newsfeed').html(data);
			else
				$('.search-data').html(data);
				var html = '<div class="ossn-menu-search"><div class="title">RESULT TYPE</div><li class="ossn-menu-search-users"><a href="search?type=users&amp;q=sf"><div class="text">People</div></a></li><li class="ossn-menu-search-groups"><a href="search?type=groups&q="><div class="text">Teams</div></a></li></div>';
				$('.coloum-left.ossn-page-contents').html(html);
				addOffsetClass('searchResults');
			
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}

/*
	DISABLE ENTER KEY IN COMMENT EDIT
*/
$(document).on('keydown', '#comment-edit', function(event){
	console.log('typed');
	if(event.which==13){
		event.preventDefault();
	}
});

/*
	NOTIFICATIONS
*/

$(document).on('click', '.ossn-notifications-all>li', function(event){
	event.preventDefault();
	var $this = $(this);
	var type = $this.attr('data-type');
	//Group join request
	if(type=='group-join'){
		var guid = $this.attr('data-group');
		viewSubpages('requests', guid);
	}
	//annotation
	else if(type==="annotation-like"){
		var category = $this.attr('data-category');
		//post like
		if(category=='post'){
			var guid = $this.attr('data-guid');
			viewLikedPost(guid, false);
		}
	}
	else if(type=='profile-pic'){
		var guid = $this.attr('data-guid');
		loadPhoto(guid, 'profile', 'out', false);
	}
	else if(type=='wall-post'){
		var guid = $this.attr('data-guid');
		viewLikedPost(guid, false);
	}
	else if(type=='group-post'){
		var guid = $this.attr('data-guid');
		viewLikedPost(guid, false);
	}
	else if(type=='group-invite'){
		alert(2);
		var group_id = $this.attr('data-group');
		viewSubpages('requests', group_id);
	}
});

function viewLikedPost(guid, isInspiration){
	var g = true;
	if(isInspiration)
		g = false;
	$.ajax({
		url: getBaseUrl() + "restCalls/application/notification/post.php",
		type: "POST",
		data: {'guid':guid, 'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		global:g,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			if(!isInspiration)
				$('.ossn-layout-newsfeed').html(data);
			else{
				$('.ossn-layout-newsfeed').hide();
				$('#comment-container').html(data)
									   .show();
				$('#comment-container').find('.comments-list').show();
				$('#comment-container').find('#comment-box-'+guid).focus();
			}
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
	
}

/*
	FUNCTION FOR TAGGING FRIENDS
*/
$(document).on('keyup', '#ossn-wall-friend-input-custom', function(event){
	var q = $('#ossn-wall-friend-input-custom').val();
	if(q!=''){
		$.ajax({
			url: getBaseUrl() + "restCalls/application/friend/friendpicker.php",
			type: "POST",
			data: {'q': q, 'session_id': getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			global:false,
			crossDomain: true,
			
			beforeSend: function(){
				$('.search-entity-holder').show();
				var html = '<li class="entity-li"><span class="entity-fullname">Searching..</span></li>';
				$('.entity-ul').html(html);
			},
			success: function(data){
				console.log(data);
				var html="";
				if(data.length===0){
					$('.search-entity-holder').show();
					var html = '<li class="entity-li"><span class="entity-fullname">No results..</span></li>';
					$('.entity-ul').html(html);
				}
				else{
					for(var i =0; data[i]!=null; i++){
					html=html+'<li class="entity-li" data-id='+data[i]['id']+'><img class="entity-profilepic" src="http://mychance.in/avatar/'+data[i]['username']+'/small"><span class="entity-fullname">'+data[i]['first_name']+' '+data[i]['last_name']+'</span></li>';
					}
					$('.search-entity-holder').show();
					$('.entity-ul').html(html);
				}
			},
			error: function(xhr, status, error){
				
			},
			complete: function(){
				
			}
		});
	}
	else{
		$('.entity-ul').html("");
		$('.search-entity-holder').hide();
	}
});

/*
	FUNCTION TO HANDLE CLICK ON SEARCH ENITITY (FRIEND TAGGING)
*/
$(document).on('click', '.entity-li', function(event){
	var $this = $(this);
	var guid = $this.attr('data-id');
	var flag = false;
	$( ".token-input-list>li" ).each(function( index ) {
		if($(this).attr('data-guid')==guid){
			flag = true;
		}
	});
	if(flag)
		return false;
	var html = '<li class="token-input-token" data-guid="'+guid+'"><p>'+$this.find('span').html()+'</p><span class="token-input-delete-token">×</span></li>';
	$('.token-input-list').append(html);
	var tagged = $('#ossn-wall-friend-input-hidden').val();
	if(tagged==''){
		tagged=guid;
	}
	else
		tagged = tagged+','+guid;
	$('#ossn-wall-friend-input-hidden').val(tagged);
	$('.entity-ul').html("");
	$('.search-entity-holder').hide();
	$('#ossn-wall-friend-input-custom').val('');
});

/*
	FUNCTION TO REMOVE TAG
*/

$(document).on('click', '.token-input-delete-token', function(event){
	var $this = $(this).parent();
	var guid = $this.attr('data-guid');
	$this.remove();
	var tagged = $('#ossn-wall-friend-input-hidden').val();
	tagged = tagged.replace(guid, '');
	if (~tagged.indexOf(",,")){
		tagged=tagged.replace(",,", ",");
	}
	if(tagged.substr(tagged.length - 1)==','){
		tagged=tagged.substr(0, tagged.length-1);
	}
	if(tagged.substr(0,1)==','){
		tagged=tagged.substr(1, tagged.length-1);
	}
	$('#ossn-wall-friend-input-hidden').val(tagged);
});

function showPostAddPage(){	
	$('.opensource-socalnetwork').hide();
	parent.modifyStyle();
	$('.owner-user-img').attr('src', window.localStorage.getItem('proimg'));
	$('.owner-user-name').html(window.localStorage.getItem('proname'));
	$('#postAddPage').addClass('slideIn').show();
	$('.ossn-wall-photo').click();
}

function hidePostAddPage(){
	$('.opensource-socalnetwork').show();
	parent.restoreStyle();
	$('#postAddPage').removeClass('slideIn').hide();
	parent.showMikeIcon();
	clearInputs();
}

function submitMainPostForm(){
	$('#wall-post-submit').click();
}

$(document).on('click', '.post-contents img', function(event){
	var $this = $(this);
	parent.showFullscreen($this.attr('src'), 'img');
});

$(document).on('click', '.control-popup', function(event){
	var $this = $(this);
	//$('.dropdown-menu.post-controls-popup').hide();
	if(!$this.next().is(':visible'))
		$this.next().fadeIn(200);
	else
		$this.next().hide(200);
});

/*HANDLE CLICK ON POST CONTROL POPUP OF NORMAL POST > STAR*/
$(document).on('click', '.post-controls-popup .post-control-star', function(event){
	var $this = $(this);
	//var guid = $this.parent().attr('id').slice(12);
	var guid = $this.attr('data-guid');
	$('#ossn-like-'+guid).click();
});

/*HANDLE CLICK ON POST CONTROL POPUP OF ENTITY POST > STAR*/
$(document).on('click', '.post-controls-popup-photo .post-control-star', function(event){
	console.log("entity");
	var $this = $(this);
	//var guid = $this.parent().attr('id').slice(12);
	var guid = $this.attr('data-guid');
	$('#ossn-like-'+guid).click();
});

/*HANDLE CLICK ON POST CONTROL POPUP OF ENTITY/NORMAL POST > COMMENT*/
/*$(document).on('click', '.post-controls-popup .post-control-inspire, .post-controls-popup-photo .post-control-inspire', function(event){
	var $this = $(this);
	var clickid = $this.parent().parent().parent().parent().parent().parent().find('.comment-post').attr('data-guid');
	ViewCommentForm(clickid);
	$this.parent().parent().hide();
});*/

$(document).on('click', '.post-controls-popup .post-control-pass', function(event){
	var $this = $(this);
	//var clickid = $this.parent().parent().parent().parent().parent().parent().find('#share-btn').click();
	var clickid = $this.attr('data-guid');
	$('#share-btn[data-guid="'+clickid+'"]').click();
	/*ViewCommentForm(clickid);*/
	$this.parent().parent().hide();
});

$(document).on('click', '.post-control-change', function(event){
	var guid = $(this).attr('data-guid');
	$('.post-control-edit.ossn-wall-post-edit[data-guid="'+guid+'"]').click();
});

$(document).on('click', '.post-control-vanish', function(event){
	var guid = $(this).attr('data-guid');
	$('.post-control-delete.ossn-wall-post-delete[data-guid="'+guid+'"]').click();
});

$(window).click(function(e) {
	if(e.target.className!='fa fa-angle-down control-popup'){
		if($('.dropdown-menu').is(':visible'))
			$('.dropdown-menu').fadeOut(150);
	}
});

$('.dropdown-menu').click(function(event){
    event.stopPropagation();
});

var position = 0;
function hideFooter(){
	position= $(window).scrollTop();
	$('.ossn-layout-newsfeed').hide();
	$('#comment-container').html('<i class="fa fa-spinner fa-spin fa-3x fa-fw" style="position: fixed;top: 47%;left: 44%;transform: translate(-50%, -50%);color: #1c3b5a;"></i>').fadeIn();
	//$('#comment-container').find("footer").hide();
	//$('.ossn-layout-newsfeed').find("footer").hide();
}
function showFooter(){
	//$('#comment-container').find("footer").show();
	//$('.ossn-layout-newsfeed').find("footer").show();
}
function postInspiration(type, guid){
	parent.hideIconsComment(1);
	if(type=='wall-post'){
		hideFooter();
		parent.hideMikeIcon();
		viewLikedPost(guid, true);
	}
	else if(type=='profile'){
		hideFooter();
		parent.hideMikeIcon();
		loadPhoto(guid, 'profile', 'out', true);
	}
	else if(type=='covers'){
		hideFooter();
		parent.hideMikeIcon();
		loadPhoto(guid, 'covers', 'out', true);
	}
}

function closeCommentViewPage(){
	showFooter()
	parent.showMikeIcon();
	$('.dropdown-menu').hide();
	$('footer').show()
	$('#comment-container').html('').fadeOut(100);
	$('.ossn-layout-newsfeed').show();
	$(window).scrollTop(position);
}

/*
*  function to invite users to a group
*/

function inviteFriends(group_id){
	parent.inviteFriends(group_id);
}

/*
*	function to share post from group to newsfeed
*/
function sharePostFromGroup(guid, group_guid){
	$.ajax({
		url: "http://m.mychance.in/share/xhr.php",
		type: "post",
		data: {'guid':guid, 'group':group_guid, 'session_id':window.localStorage.getItem('session_id')},
		beforeSend: function(){
			$('.confirm').hide();
			$('body').css('pointer-events', 'none').css('opacity', '0.4');
		},
		success: function(data){
				console.log(data);
				$('#activity-item-'+guid).find('.row').first().append('<div class="alert alert-success"> <strong>Success!</strong> Post successfully passed to friends. </div>');
				window.setTimeout(function(){
					$('.alert-success').fadeOut(1000).remove();
				}, 3000);				
		},
		error:function(req, err,error){
			$('#activity-item-'+guid).find('.row').first().append('<div class="alert alert-danger"> <strong>Failed!</strong> Post cannot be passed to friends right now. </div>');
				window.setTimeout(function(){
					$('.alert-danger').fadeOut(1000).remove();
				}, 3000);
		},
		complete:function(){
			$('body').css('pointer-events', 'auto').css('opacity', '1');
			$('#no-btn').click();
		}
	});
}



