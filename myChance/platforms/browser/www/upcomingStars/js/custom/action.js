


var username_soc = '';

function getBaseUrl(){
	var baseUrl = "http://m.mychance.in/";
	return baseUrl;
}
function getLocalStorageItem(key){
	return window.localStorage.getItem(key);
}

function removeLocalStorageItem(key){
	window.localStorage.removeItem(key);
}

function session_out(){
	removeLocalStorageItem('session_id');
}

function getSessionId(){
	session_out();
	var session_id = getLocalStorageItem("session_id");
	if(session_id=='' || session_id===null || session_id==undefined){
		var userid = getLocalStorageItem('userid');
		var username = getLocalStorageItem("username");
		var hash_session = $.md5(userid+username);
		window.localStorage.setItem("session_id", hash_session);
		console.log("session id generated: "+ hash_session);
		return $.md5(userid+username);
	}
	else
		return session_id;
}

function getParam(url ,name){
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(url);
    if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

function disableClicks($this){
	$this.css('pointer-events', 'none')
		 .css('opacity', '0.5');
}

function enableClicks($this){
	$this.css('pointer-events', 'auto')
		 .css('opacity', '1');
}

function confirmOperation(){
	return confirm('Do you really want to perform this operation?');
}

$(document).on('ajaxStart', function() {
	$('.loader-wrapper').show();
	$('.no-loding').css('pointer-events', 'none')
				   .css('opacity', '0.4');
});

$(document).on('ajaxComplete', function() {
	$('.loader-wrapper').hide();
	$('.no-loding').css('pointer-events', 'auto')
				   .css('opacity', '1');
});

/*
	FUNCTION FOR HANDLING CLICK ON USER FULL NAME HREF / TIMELINE ON USER PROFILE
*/

$(document).on('click', '.owner-link, .userlink', function(event){
	event.preventDefault();
	var string = $(this).attr('href');
	var username = string.substring(string.lastIndexOf("/") + 1);
	username_soc = username;
	$.ajax({
		url: getBaseUrl()+"restCalls/application/user/profile.php",
		type: "POST",
		data: {"username": username, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			//alert(data);
			$('.ossn-layout-newsfeed').html(data);
		},
		error: function(xhr, status, error){
			alert(xhr.responseText);
		},
		complete: function(){
		}
	});
	
});

/*
	FUNCTION FOR VIEWING USER'S FRIEND LIST
*/

$(document).on('click', '.menu-user-timeline-friends', function(event){
	event.preventDefault();
	var guid = $(this).attr('data-guid');
	viewFriends(guid);
});

function viewFriends(guid){
	$.ajax({
		url: getBaseUrl()+"restCalls/application/user/friends.php",
		type: "POST",
		data: {"guid": guid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			//alert(data);
			$('.ossn-layout-newsfeed').html(data);
		},
		error: function(xhr, status, error){
			alert(xhr.responseText);
		},
		complete: function(){
		}
	});
}

/*
	FUNCTION FOR VIEWING USER'S PHOTOS
*/

$(document).on('click', '.menu-user-timeline-photos', function(event){
	event.preventDefault();
	var guid = $(this).attr('data-guid');
	viewPhotos(guid)
});

function viewPhotos(guid){
	$.ajax({
		url: getBaseUrl()+"restCalls/application/user/photos.php",
		type: "POST",
		data: {"guid": guid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			//alert(data);
			$('.ossn-layout-newsfeed').html(data);
		},
		error: function(xhr, status, error){
			alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}

/**
	FUNCTION FOR ADDING WALL POST
**/
$(document).on('submit', '#ossn-wall-form', function(e) {
	try{
		e.preventDefault();
		var ajaxUrl = getBaseUrl()+"restCalls/application/wall/post/user.php";
		var data = new FormData($(this)[0]);
		data.append("session_id", getLocalStorageItem('session_id'));
		var wallowner = $(this).find("input[name='wallowner']").val();
		if(wallowner==null || wallowner=='' || wallowner==undefined)
			ajaxUrl=getBaseUrl()+"restCalls/application/wall/post/home.php";
		
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data: data,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			crossDomain: true,
			
			beforeSend: function(){
				//$('#wall-post-submit').hide();
				//$('.ossn-loading').removeClass("ossn-hidden");
			},
			success: function(data){
				//console.log(data);
				if(data!=0){
					$('.user-activity').prepend(data);
				}
				else
					alert("Cannot create post! Pleas try again later..");
			},
			error: function(xhr, status, error){
				alert(xhr.responseText);
			},
			complete: function(){
				//$('.ossn-loading').addClass("ossn-hidden");
				//$('#wall-post-submit').show();
				$('#post').val('');
			}
		});
	}
	catch(e){
		alert(e.message);
	}
});



/*$(document).on('submit', '.comment-box', function(e) {
	try{
		e.preventDefault();
		alert("caight");
		return;
		var data = new FormData($(this)[0]);
		$.ajax({
			url: getBaseUrl()+"restCalls/application/wall/post/home.php",
			type: "POST",
			data: data,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			crossDomain: true,
			
			beforeSend: function(){
				$('#wall-post-submit').hide();
				$('.ossn-loading').removeClass("ossn-hidden");
			},
			success: function(data){
				console.log(data);
				data = $.parseJSON(data);
				console.log(data.test);
				if(!data.hasOwnProperty("errmessage")){
					//alert(console.log(data.template));
					$('.user-activity').prepend(data.template);
				}
				else
					alert(data.errmessage);
			},
			error: function(xhr, status, error){
				alert(xhr.responseText);
			},
			complete: function(){
				$('.ossn-loading').addClass("ossn-hidden");
				$('#wall-post-submit').show();
				$('#post').val('');
			}
		});
	}
	catch(e){
		alert(e.message);
	}
});*/

/*
	FUNCTION FOR DELETING WALL POST
*/

 $(document).on('click', '.ossn-wall-post-delete', function(e) {
	$url = $(this);
	e.preventDefault();
	$.ajax({
			url: getBaseUrl() + "restCalls/application/wall/post/delete.php", //$url.attr('href')+"&session_id="+getLocalStorageItem('session_id'),
			type: "POST",
			data:{"post": $url.attr('data-guid'), "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				$('#activity-item-' + $url.attr('data-guid')).attr('style', 'opacity:0.5;');
			},
			success: function(callback){
				//alert(callback);
				if (callback == 1) {
					$('#activity-item-' + $url.attr('data-guid')).fadeOut();
					$('#activity-item-' + $url.attr('data-guid')).remove();
				} else {
					$('#activity-item-' + $url.attr('data-guid')).attr('style', 'opacity:1;');
				}
			},
			error: function(xhr, status, error){
				alert("error:"+xhr.responseText);
			},
			complete: function(){
				
			}
	});
});


/***** FUNTION FOR EDITING POST AND COMMENT, ADDING NEW ALBUM AND NEW PHOTO ******/

$(document).on('submit', '.ossn-form:not("#ossn-wall-form, .comment-container")', function(e) {
	//if($(this).parent().parent().hasClass('ossn-box-inner')){
		e.preventDefault();
		var $this = $(this);
		var data = new FormData($(this)[0]);
		var guid = $(this).parent().find(('input[name=guid]')).val();
		var url = getBaseUrl() + $(this).attr('data-action');
		data.append("session_id" , getLocalStorageItem('session_id'));
		
		$.ajax({
			url: url ,
			type: "POST",
			data:data,
			async: true,
			cache: false,
			contentType: false,
			processData: false,
			crossDomain: true,
			
			beforeSend: function(){
				disableClicks($this.find("input[type='submit']"));
			},
			success: function(data){
				//console.log(data);
				var obj = $.parseJSON(data);
				//alert("data:"+obj.post);
				
				if(!obj.hasOwnProperty('err') && obj.type=='wall')
					$('#activity-item-'+guid).find('.post-contents').find('p').html(obj.text);
				else if(!obj.hasOwnProperty('err') && obj.type=='comment')
					$('#comments-item-'+guid).find('.comment-contents').find('p').html(obj.text);
				else if(!obj.hasOwnProperty('err') && obj.type=='album_add')
					$('.row.ossn-profile-bottom').html(obj.text);
				else if(!obj.hasOwnProperty('err') && obj.type=='photo')
					$('.ossn-profile.container').html(obj.text);
				else if(!obj.hasOwnProperty('err') && obj.type=='profileSave'){
					if(obj.hasOwnProperty('err-pro')){
						alert(obj.errpro);
					}
					else{
						$('.row.ossn-profile-bottom').html(obj.text);
						 $('html, body').animate({scrollTop:0}, 'slow');
					}
					
				}
				else if(obj.hasOwnProperty('err'))
					console.log(obj.err);
				Ossn.MessageBoxClose();
			},
			error: function(xhr, status, error){
				alert("error:"+xhr.responseText);
			},
			complete: function(){
				enableClicks($this.find("input[type='submit']"));
			}
		});
	//}
});
/**
	FUNTION FOR ADDING COMMENT
**/
$(document).on('submit', '.ossn-form.comment-container', function(event) {
	event.preventDefault();
	var data = new FormData($(this)[0]);
	$container = $(this).find("input[name=post]").val();
	if($container==null || $container==undefined || $container=='')
		$container = $(this).find("input[name=entity]").val();
	data.append("session_id" , getLocalStorageItem('session_id'));
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/post/comment.php",
		type: "POST",
		data:data,
		async: true,
		cache: false,
		contentType: false,
		processData: false,
		crossDomain: true,
		
		beforeSend: function(){
			$('#comment-box-' + $container).attr('readonly', 'readonly');
		},
		success: function(data){
			callback = data;
			if (callback['process'] == 1) {
				$('#comment-box-' + $container).removeAttr('readonly');
				$('#comment-box-' + $container).val('');
				$('.ossn-comments-list-' + $container).append(callback['comment']);
				$('#comment-attachment-container-' + $container).hide();
				$('#ossn-comment-attachment-' + $container).find('.image-data').html('');
				//commenting pic followed by text gives warnings #664 $dev.githubertus
				$('#comment-container-' + $container).find('input[name="comment-attachment"]').val('');
			}
			if (callback['process'] == 0) {
				$('#comment-box-' + $container).removeAttr('readonly');
				Ossn.MessageBox('syserror/unknown');
			}
				
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
			//$('#activity-item-'+)
		},
		complete: function(){
			
		}
	});
});
/****** FUNCTIN FOR DELETEING COMMENT ****/

$(document).on('click', '.ossn-delete-comment', function(event){
	event.preventDefault();
	var url = $(this).attr('href');
	$comment_id = getParam(url, 'comment');
	
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/post/comment_delete.php",
		type: "POST",
		data:{"comment": $comment_id, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$('#comments-item-' + $comment_id).attr('style', 'opacity:0.6;');
		},
		success: function(data){
			callback = data;
			if (callback == 1) {
				$('#comments-item-' + $comment_id).fadeOut().remove();
			}
			if (callback == 0) {
				$('#comments-item-' + $comment_id).attr('style', 'opacity:0.6;');
			}
				
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
			//$('#activity-item-'+)
		},
		complete: function(){
			
		}
	});
});

/*
	FUNCTON FOR LIKEING WALL POST
*/
function OssnPostLike(post){
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/post/Like/like.php",
		type: "POST",
		data:{"post": post, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			 $('#ossn-like-' + post).html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
			//console.log(callback);
            if (callback['done'] !== 0) {
                $('#ossn-like-' + post).html(callback['button']);
                $('#ossn-like-' + post).attr('onClick', 'OssnPostUnlike(' + post + ');');
            } else {
                $('#ossn-like-' + post).html(Ossn.Print('like'));
            }
			
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
	
}

/*
	FUNCTION FOR UNLIKING WALL POST
*/

function OssnPostUnlike(post){
	$.ajax({
		url: getBaseUrl() + "restCalls/application/wall/post/Like/unlike.php",
		type: "POST",
		data:{"post": post, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			 $('#ossn-like-' + post).html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
			console.log(callback);
            if (callback['done'] !== 0) {
                $('#ossn-like-' + post).html(callback['button']);
                $('#ossn-like-' + post).attr('onclick', 'OssnPostLike(' + post + ');');
            } else {
                $('#ossn-like-' + post).html(Ossn.Print('unlike'));
            }
			
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
}
/*
	FUNCTION FOR LIKING ENTITY
*/

OssnEntityLike = function(postid) {
	$.ajax({
		url: getBaseUrl() + 'restCalls/application/wall/post/Like/like.php',
		type: "POST",
		data:{"entity": postid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$("#ossn-like-" + postid).html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
			console.log(callback);
            if (callback['done'] !== 0) {
                $("#ossn-like-" + postid).html(callback['button']);
                $("#ossn-like-" + postid).attr('onClick', 'OssnEntityUnlike(' + postid + ');');
            } else {
                $("#ossn-like-" + postid).html(Ossn.Print('like'));
            }
			
		},
		error: function(xhr, status, error){
			$("#ossn-like-" + postid).html(Ossn.Print('like'));
			alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
};

/*
	FUNCTION FOR UNLIKING ENTITY
*/

OssnEntityUnlike = function(postid) {
	$.ajax({
		url: getBaseUrl() + 'restCalls/application/wall/post/Like/unlike.php',
		type: "POST",
		data:{"entity": postid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$('#ossn-like-' + postid).html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
			 if (callback['done'] !== 0) {
                $('#ossn-like-' + postid).html(callback['button']);
                $('#ossn-like-' + postid).attr('onclick', 'OssnEntityLike(' + postid + ');');
            } else {
                $('#ossn-like-' + postid).html(Ossn.Print('unlike'));
            }
			
		},
		error: function(xhr, status, error){
			$("#ossn-like-" + postid).html(Ossn.Print('Unlike'));
			alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
};

/*
	FUNCTION FOR LIKING/UNLIKING COMMENT
*/
$(document).on('click', '.ossn-like-comment', function(event){
	event.preventDefault();
	$item = $(this);
	var ajaxUrl = getBaseUrl() + "restCalls/application/wall/post/Like/annotation_like.php";
	$annotation = getParam($item.attr('href'),'annotation');
	var $type = $.trim($item.attr('data-type'));
	if($type == 'Unlike')
		ajaxUrl = getBaseUrl() + "restCalls/application/wall/post/Like/annotation_unlike.php"
    var $url = $item.attr('href');
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"annotation": $annotation, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$item.html('<img src="' + Ossn.site_url + 'components/OssnComments/images/loading.gif" />');
		},
		success: function(callback){
			
			if (callback['done'] == 1) {
				$total_guid = Ossn.UrlParams('annotation', $url);
				$total = $('.ossn-total-likes-' + $total_guid).attr('data-likes');
				if ($type == 'Like') {
					$item.html(Ossn.Print('unlike'));
					$item.attr('data-type', 'Unlike');                            
					var unlike = $url.replace("like", "unlike");
					$item.attr('href', unlike);
					$total_likes = $total;
					$total_likes++;
					$('.ossn-total-likes-' + $total_guid).attr('data-likes', $total_likes);
					$('.ossn-total-likes-' + $total_guid).html('<i class="fa fa-thumbs-up"></i>' + $total_likes);
				}
				if ($type == 'Unlike') {
					$item.html(Ossn.Print('like'));
					$item.attr('data-type', 'Like');                            
					var like = $url.replace("unlike", "like");
					$item.attr('href', like);
					if ($total > 1) {
						$like_remove = $total;
						0
						$like_remove--;
						$('.ossn-total-likes-' + $total_guid).attr('data-likes', $like_remove);
						$('.ossn-total-likes-' + $total_guid).html('<i class="fa fa-thumbs-up"></i>' + $like_remove);
					}
					if ($total == 1) {
						$('.ossn-total-likes-' + $total_guid).attr('data-likes', 0);
						$('.ossn-total-likes-' + $total_guid).html('');
	
					}
				}
			}
			if (callback['done'] == 0) {
				if ($type == 'Like') {
					$item.html(Ossn.Print('like'));
					$item.attr('data-type', 'Like');
					Ossn.MessageBox('syserror/unknown');
				}
				if ($type == 'Unlike') {
					$item.html(Ossn.Print('unlike'));
					$item.attr('data-type', 'Unlike');
					Ossn.MessageBox('syserror/unknown');
	
				}
			}
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
			//$('#activity-item-'+)
		},
		complete: function(){
			
		}
	});
});

/*
	FUNCTION FOR ADD/REMOVE FRIEND
*/

$(document).on('click', '.friend-add-a', function(event){
	event.preventDefault();
	var $this = $(this);
	var ajaxUrl = getBaseUrl() + "restCalls/application/friend/add.php";
	if($this.attr('data-action')=='remove')
		ajaxUrl = getBaseUrl() + "restCalls/application/friend/remove.php";
	var add_guid = getParam($this.attr('href'), 'user');
	$this.addClass('disableClick');
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"user": add_guid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(callback){
			//alert(callback);
			var obj = $.parseJSON(callback);
			if(obj.type==1){
				if($this.attr('data-action')=='add'){
					if($this.hasClass('btn-primary')){
						$this.removeClass('btn-primary')
							 .addClass('btn-danger')
							 .html('Unfriend')
							 .attr('data-action', 'remove');
					}
					else
						$this.html('Unfriend')
							 .attr('data-action', 'remove');
				}
				else{
					if($this.hasClass('btn-danger')){
						$this.removeClass('btn-danger')
							 .addClass('btn-primary')
							 .html('Add friend')
							 .attr('data-action', 'add');
					}
					else
						$this.html('Add friend')
							 .attr('data-action', 'add');
				}
			}
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
		},
		complete: function(){
			$this.removeClass('disableClick');
		}
	});
});

/*
	FUNCTION FOR POKING/BlOCKING/UNBLOCKING USER
*/
$(document).on('click', '.profile-menu-extra-poke, .profile-menu-extra-block', function(event){
	event.preventDefault();
	var $this = $(this);
	var ajaxUrl ='';
	var guid = getParam($this.attr('href'), 'user');
	$this.addClass('disableClick');
	
	//Poke
	if($this.hasClass('profile-menu-extra-poke')){
		ajaxUrl = getBaseUrl() + "restCalls/application/user/poke.php";
	}
	//Block//UnBlock
	else if($this.hasClass('profile-menu-extra-block')){
		if($this.html()=='Block')
			ajaxUrl = getBaseUrl() + "restCalls/application/user/block.php";
		else
			ajaxUrl = getBaseUrl() + "restCalls/application/user/unblock.php";
	}
	if(ajaxUrl!=''){
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data:{"user": guid, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				
			},
			success: function(callback){
				alert(callback);
				var obj = $.parseJSON(callback);
				if(obj.done==1){
					if($this.html()=='Poke'){
						/*
							Handle Poke Success
						*/
					}
					else if($this.text()=='Block'){
						$this.text('Unblock');
					}
					else if($this.text()=='Unblock'){
						$this.text('Block');
					}
				}
			},
			error: function(xhr, status, error){
				alert("error:"+xhr.responseText);
			},
			complete: function(){
				$this.removeClass('disableClick');
			}
		});
	}
});

/*
	FUNCTION FOR VIEWING USER PHOTOS -- HANDLES CLICK ON ALBUM
*/

var local_guid;
var local_type;

$(document).on('click', '.album-thumbnail', function(event){
	event.preventDefault();
	var $this = $(this);
	var guid = $this.attr('data-guid');
	local_guid = guid;
	var type = $this.attr('data-type');
	local_type = type;
	var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/album/view_photos.php"
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"guid": guid, "type": type, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(callback){
			$('.ossn-profile.container').html(callback).append("<input type='hidden' value='"+guid+"' id='album-guid'>");
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
	
});

/*
	FUNCTION FOR VIEWING PHOTO IN AN ANLBUM - HANDLES CLICK ON INDIVIDUAL PHOTO
*/
$(document).on('click', '.pthumb.image-view', function(event){
	event.preventDefault();
	var $this = $(this).parent();
	var guid = $this.attr('data-guid');
	var type = $this.attr('data-type');
	var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/photo/view_photo.php"
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"guid": guid, "type": type, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(callback){
			$('.ossn-profile.container').html(callback);
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
});

/*
	FUNCTION TO GO BACK TO ALBUM VIEW
*/
$(document).on('click', '.button-grey.back-button', function(event){
	event.preventDefault();
	if(username_soc!='' && username_soc!=undefined && username_soc!=null)
		$.ajax({
			url: getBaseUrl() + "restCalls/application/user/photo/album/back_to_album.php",
			type: "POST",
			data:{"username":username_soc, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				
			},
			success: function(callback){
				$('.ossn-layout-newsfeed').html(callback);
			},
			error: function(xhr, status, error){
				alert("error:"+xhr.responseText);
			},
			complete: function(){
				
			}
		});
	else{
		location.reload();
	}
	
});

/*
	FUNCTION TO GO BACK TO PHOTOS VIEW - INSIDE AN ALBUM
*/

$(document).on('click', '.back-to-album', function(event){
	event.preventDefault();
	var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/album/view_photos.php";
	if(local_guid!='' && local_guid!=null && local_guid!=undefined && local_type!='' && local_type!=null && local_type!=undefined){
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data:{"guid": local_guid, "type": local_type, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				
			},
			success: function(callback){
				$('.ossn-profile.container').html(callback);
			},
			error: function(xhr, status, error){
				alert("error:"+xhr.responseText);
			},
			complete: function(){
				
			}
		});
	}
	else
		location.reload();
});

/*
	FUNCTION FOR VIEWING PHOTO ADD DIALOG BOX
*/

$(document).on('click', '#ossn-add-photos', function(e){
	var $dataurl = local_guid; //$('#album-guid').val();
	var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/photos/add_form.php";
	$.ajax({
		url: ajaxUrl,
		type: "POST",
		data:{"album": $dataurl, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$('.ossn-halt').addClass('ossn-light');
			$('.ossn-halt').attr('style', 'height:' + $(document).height() + 'px;');
			$('.ossn-halt').show();
			$('.ossn-message-box').html('<div class="ossn-loading ossn-box-loading"></div>');
			$('.ossn-message-box').fadeIn('slow');
		},
		success: function(callback){
			$('.ossn-message-box').html(callback).fadeIn();
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
		},
		complete: function(){
			
		}
	});
}); 

/*
	FUNCTION FOR DELETING ALBUM
*/

$(document).on('click', '.button-grey.delete-album', function(event){
	event.preventDefault();
	if(confirmOperation()){
		var $this = $(this);
		var guid = $this.attr('data-guid');
		var username = username_soc;
		var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/album/delete.php";
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data:{"guid": guid, "username": username, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				disableClicks($this);
			},
			success: function(callback){
				$('.ossn-layout-newsfeed').html(callback);
			},
			error: function(xhr, status, error){
				alert("error:"+xhr.responseText);
			},
			complete: function(){
				enableClicks($this);
			}
		});
	}
}); 

/*
	FUNCTION FOR DELETING A PHOTO
*/

$(document).on('click', '.btn-danger.delete-photo', function(event){
	event.preventDefault();
	if(confirmOperation()){
		var $this = $(this);
		var album_type = 'view';
		if($this.hasClass('profile'))
			album_type = 'profile';
		else if($this.hasClass('cover'))
			album_type = 'cover';
		var id = getParam($this.attr('href'), 'id');
		var ajaxUrl = getBaseUrl() + "restCalls/application/user/photo/photo/delete.php";
		$.ajax({
			url: ajaxUrl,
			type: "POST",
			data:{"id": id, 'album_type':album_type, "guid": local_guid, "type":local_type, "session_id": getLocalStorageItem('session_id')},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				disableClicks($this);
			},
			success: function(callback){
				$('.ossn-profile.container').html(callback);
			},
			error: function(xhr, status, error){
				alert("error:"+xhr.responseText);
			},
			complete: function(){
				enableClicks($this);
			}
		});
	}
});

/*
	FUNCTION FOR UPDATING USER INFO - HANDLES CLICK ON UPDATE INFO BUTTON IN USER TIMELINE
*/

$(document).on('click', '.btn-action.update-user-info', function(event){
	event.preventDefault();
	var $this = $(this);
	var guid = $this.attr('data-guid'); 	
	$.ajax({
		url: getBaseUrl()+"restCalls/application/user/profile_edit.php",
		type: "POST",
		data:{"guid": guid, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			disableClicks($this);
		},
		success: function(callback){
			$('.row.ossn-profile-bottom').html(callback);
		},
		error: function(xhr, status, error){
			alert("error:"+xhr.responseText);
		},
		complete: function(){
			enableClicks($this);
		}
	});
});

$(document).on('click', '.masonry-item img', function(event){
	var $this = $(this);
	parent.showFullscreen($this.attr('src'), 'img');
});



