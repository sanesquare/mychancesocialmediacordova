var deviceId;
var login;
var url = "http://m.mychance.in";
var storage = window.localStorage;
var fresh = false;
var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);

    },
    onDeviceReady: function() {
		var permissions = cordova.plugins.permissions;
		permissions.hasPermission(permissions.READ_EXTERNAL_STORAGE, checkPermissionCallback, null);

        function checkPermissionCallback(status) {
            if (!status.hasPermission) {
                var errorCallback = function () {
                    console.warn('Storage permission is not turned on');
                }
                permissions.requestPermission(
                  permissions.READ_EXTERNAL_STORAGE,
                  function (status) {
                      if (!status.hasPermission) {
                          errorCallback();
                      } else {
                          // continue with downloading/ Accessing operation 
                          //$scope.downloadFile();
                      }
                  },
                  function(){console.log('Permission denied');});
            }
        }
        document.addEventListener("pause", checkMessenger, false);
        document.addEventListener("resume", checkMessenger, false);
		document.addEventListener("backbutton", onBackKeyDown, false);
        checkMessenger();
        fresh = true;
		
			var push = PushNotification.init({
			android: {
				"senderID": '1012566166093 ',
				vibrate: false,
				sound: true,
				iconColor: "greay",
				icon: "notification_icon"
			},
			browser: {
				pushServiceURL: 'http://push.api.phonegap.com/v1/push'
			},
			ios: {
				alert: "true",
				badge: "true",
				sound: "true"
			},
			windows: {}
		});
		
		push.on('registration', function(data) {
			deviceId = data.registrationId;
            var userName = storage.getItem("userName");
            var passWord = storage.getItem("passWord");
            var oldToken = storage.getItem("oldToken");
            try {
                if (userName.length > 0 && passWord.length > 0)
                    loginServer(userName, passWord, oldToken, deviceId);
                else {
                    $("#loginPage").css("display", "block");
                }
            } catch (e) {
                $("#loginPage").css("display", "block");
            }
		});
		
		push.on('notification', function(data) {
			// data.message,
			// data.title,
			// data.count,
			// data.sound,
			// data.image,
			// data.additionalData
		});
		
		push.on('error', function(e) {
			alert(e.message);
		});
		
		//push notification
		/*var push = PushNotification.init({
		  android: {
			senderID: '1058688935486'
		  }
		});

        push.on('registration', function(data) {
            deviceId = data.registrationId;
            var userName = storage.getItem("userName");
            var passWord = storage.getItem("passWord");
            var oldToken = storage.getItem("oldToken");
            try {
                if (userName.length > 0 && passWord.length > 0)
                    loginServer(userName, passWord, oldToken, deviceId);
                else {
                    $("#loginPage").css("display", "block");
                }
            } catch (e) {
                $("#loginPage").css("display", "block");
            }
        });

        push.on('notification', function(data) {
            // data.message,
            // data.title,
            // data.count,
            // data.sound,
            // data.image,
            // data.additionalData
        });

        push.on('error', function(e) {
            // e.message
        });*/
        if (navigator.connection.type == 'none') {
            $('#contentIframe').attr('src', "error.html");
        } else {
            //$('#contentIframe').attr('src', "http://demo.snogol.net/mychance.in");
        }
    },
    receivedEvent: function(id) {},
};
