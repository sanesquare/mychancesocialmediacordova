//handle push notifications

//Liking wallpost
function sendLikeNotification(guid, is_entity){
	if(guid==null || guid=='undefined' || guid=='')
		return false;
	$.ajax({
		url: "http://innathecinema.com/pay/mychance/push/xhr.php",
		type: "post",
		data: {'guid':guid, 'entity':is_entity, 'myid':window.localStorage.getItem('userid'), 'action': 'wall_like'},
		global:false,
		beforeSend: function(){
			
		},
		success: function(data){
			console.log(data);
		},
		error:function(req, err,error){
			
		},
		complete:function(){
			
		}
	});
}

//comment
function sendCommentNotification(guid, is_entity){
	if(guid==null || guid=='undefined' || guid=='')
		return false;
	$.ajax({
		url: "http://innathecinema.com/pay/mychance/push/xhr.php",
		type: "post",
		data: {'guid':guid, 'entity':is_entity, 'myid':window.localStorage.getItem('userid'), 'action': 'wall_comment'},
		global:false,
		beforeSend: function(){
			
		},
		success: function(data){
			console.log(data);
		},
		error:function(req, err,error){
			
		},
		complete:function(){
			
		}
	});
}

//is_sending = 1, if sending request & is_sending=0 if accpeting request
function sendFriendRequestNotification(guid, is_sending){
	if(guid==null || guid=='undefined' || guid=='')
		return false;
	$.ajax({
		url: "http://innathecinema.com/pay/mychance/push/xhr.php",
		type: "post",
		data: {'guid':guid, 'action': 'friend_request', 'is_sending':is_sending, 'myid':window.localStorage.getItem('userid')},
		global:false,
		beforeSend: function(){
			
		},
		success: function(data){
			console.log(data);
		},
		error:function(req, err,error){
			
		},
		complete:function(){
			
		}
	});
}

function sendGroupInvitationNotification(friends){
	if(friends.length<=0)
		return false;
	$.ajax({
		url: "http://innathecinema.com/pay/mychance/push/xhr.php",
		type: "post",
		data: {'friends':JSON.stringify(friends), 'action': 'group_invitation','myid':window.localStorage.getItem('userid')},
		global:false,
		beforeSend: function(){
			
		},
		success: function(data){
			console.log(data);
		},
		error:function(req, err,error){
			
		},
		complete:function(){
			
		}
	});
}