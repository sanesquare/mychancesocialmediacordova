

//override console writing; show log only for debug=true

var consoleHolder = console;
function debug(bool){
    if(!bool){
        consoleHolder = console;
        console = {};
        console.log = function(){};
    }else
        console = consoleHolder;
}
debug(true);


/**
 *	Function to check messenger appplication available or not in device
 *	if available -> show 'showMessenger' button else -> show 'downloadMessenger' button
 */
function checkMessenger() {
    //document.getElementById("contentIframe").contentDocument.location.reload(true);
    appAvailability.check(
        'com.mychance.chat', // Package Name
        function() { // Success callback
            $(".showMessenger").show();
            $(".downloadMessenger").hide();
        },
        function() { // Error callback
            $(".downloadMessenger").show();
            $(".showMessenger").hide();
        }
    );
}
function onBackKeyDown(){
  var userName = storage.getItem("userName");
  var passWord = storage.getItem("passWord");
  if (userName.length > 0 && passWord.length > 0){
	  if($(".friendRequestDropDown").is(':visible'))
		$(".friendRequestDropDown").hide();
	else if($(".notificationDropDown").is(':visible')){
		$(".notificationDropDown").hide();
	}else{
	  try{
		CameraPreview.stopCamera();
	  }catch(e){}
	location.reload(); 
	}
  }else{
	navigator.notification.confirm(
		'Quit application?', // message
		 onConfirm,            // callback to invoke with index of button pressed
		'Confirm',           // title
		['Yes','No']     // buttonLabels
	);
  }
}
function onConfirm(buttonIndex) {
	if(buttonIndex==1){
		navigator.app.exitApp();
	}
}


/*
	Function to check network connection on every second
*/
setInterval(onlineOffline, 1000);

/*
	Function to check internet connection
*/
function onlineOffline() {
    var url = document.getElementById("contentIframe").src;
    if (navigator.connection.type == 'none') {
        login = false;
        // if offline
        if (url.indexOf("error.html") <= 0) {
            // if not showing error page
            $('#contentIframe').attr('src', "error.html");
            $(".myTitleBar").css("display", "none");
        }
        return false;
    } else {
        // if online
        if (url.indexOf("error.html") > 0) {
            // if currently displaying erro page; login me in
            var userName = storage.getItem("userName");
            var passWord = storage.getItem("passWord");
            var oldToken = storage.getItem("oldToken");
            var newToken = storage.getItem("newToken");
            if (userName.length > 0 && passWord.length > 0)
                loginServer(userName, passWord, oldToken, newToken);
            else {
                $("#loginPage").css("display", "block");
            }
        }
        return true;
    }
}

/*
	function to hide message after 3 seconds
*/
setTimeout(function() {
    $("#errorMsg").hide(500);
}, 3000);

/**
	Function to call notification and request check method on every 5 seconds
*/
setInterval(function() {
    if (login)
        checkNotificationsAndRequests();
}, 5000);

app.initialize();

/*
	Function invoked when login button clicks
*/
$("#login").click(function(e) {
	$('#successMsg').html("").hide();
    fresh = false;
    var userName = $("#userName").val();
    var passWord = $("#password").val();
    var oldToken = storage.getItem("oldToken");
    var newToken = deviceId;
    if (userName != '' && passWord != '') {
        loginServer(userName, passWord, oldToken, newToken);
    } else {
        if (userName == '')
            $("#userName").focus();
        else
            $("#password").focus();
    }
});
var id;

/**
	Login user with server
*/
function loginServer(userName, passWord, oldToken, newToken) {
	$.ajax({
        type: "GET",
        crossDomain: true,
        url: url + "/restCalls/firstLogin.php",
		contentType:"application/json; charset=UTF-8",
        data: {
            "userName": userName,
            "password": passWord,
            "oldToken": oldToken,
            "newToken": newToken
        },
		beforeSend: function(){
			$('#login').css('pointer-events', 'none')
					   .css('opacity', '0.4');
		},
        success: function(data) {
			var obj = $.parseJSON(data);
            if (!$.isEmptyObject(obj)) {
				if(obj.is_activated==0){
					$('#verify-guid').val(obj.id);
					sendCode(obj.id);
					showVerificationPage();
					return false;
				}
				var name = obj.fullName;
                id = obj.id;
                $(".myName").text(name);
				var imgsrc = url + "/avatar/" + userName + "/small";
                $(".userProPic").attr("src", imgsrc);
                $(".myTitleBar").css("display", "block");
				setImageandName(imgsrc, name);
                $("#loginPage").css("display", "none");
				 storage.setItem("userName", userName);
                storage.setItem("usertype", obj.type);				
                storage.setItem("userid", id);				
                storage.setItem("passWord", passWord);
                storage.setItem("oldToken", deviceId);
                storage.setItem("newToken", "");
				var groups = obj.groups;
				for(var i =0; groups[i]!=null; i++){
					var $group = groups[i];
					$('#groupList').prepend('<li class="groupListli" id="groupLi'+$group['guid']+'"><a href="#" onclick="viewGroup('+$group['guid']+')"><i class="fa fa-angle-right" style="font-size: 15px !important;color: #3278b3 !important;"></i>'+$group['text']+'</a></li>');
				}
               // $('#contentIframe').attr('src', url + "/restCalls/login.php?userName=" + userName + "&password=" + passWord + "&newToken=" + newToken);
			    $('#contentIframe').attr('src','socialMedia/social_media.html');
                $("#contentIframe").css("display", "block");
				//$('.mike-icon').show();
                login = true;
                // store credentials to local storage
               
            } else {
                //console.log("login failed");
                $("#errorMsg").text("Invalid Login");
                $("#errorMsg").css("display", "block");
                $("#loginPage").css("display", "block");
            }


        },
        error: function() {
            $("#errorMsg").text("Invalid Login");
            $("#errorMsg").css("display", "block");
        },
		complete:function(){
			$('#login').css('pointer-events', 'auto')
					   .css('opacity', '1');
		}
    });

}

function testing(){
	alert('this is testing');
}

/**
 *	function to check if the user session is active or not
 */
function checkSession() {
    onlineOffline();
    if (login) {
        var userName = storage.getItem("userName");
        var newToken = storage.getItem("oldToken");
        checkMySession(userName, newToken);
    }
}

/*
	Function to check user's session
*/
function checkMySession(userName, newToken) {
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/checkLogin.php",
        data: {
            "userName": userName,
            "deviceId": newToken
        },
        success: function(data) {
            if (data == "1") {
                // checkNotificationsAndRequests();
                login = true;
            } else {
                $("#contentIframe").css("display", "none");
                $('#contentIframe').attr('src', "");
                $("#loginPage").css("display", "block");
                if (login) {
                    $("#errorMsg").text("Logout Successful.");
                    $("#errorMsg").css("display", "block");
                }
                // remove un,pwd & token from localStorage
                storage.removeItem("userName");
                storage.removeItem("passWord");
                storage.removeItem("newToken");
                storage.removeItem("oldToken");
				storage.removeItem("usertype");
				storage.removeItem("userid");

                login = false;
            }
        },
        error: function() {}
    });
}

/*
	Function to check new unread notifications, messages & requests
*/
function checkNotificationsAndRequests() {
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/notifications.php",
        data: {
            "id": id
        },
        success: function(data) {
            var obj = $.parseJSON(data);
            //alert(JSON.stringify(data));
            if (parseInt(obj.friends) > 0) {
                $("#frndRqstBdge").text(obj.friends);
                $("#frndRqstBdge").show();
            } else

                $("#frndRqstBdge").hide();
            if (parseInt(obj.notification) > 0) {
                $("#ntfctnBdg").text(obj.notification);
                $("#ntfctnBdg").show();
            } else
                $("#ntfctnBdg").hide();
            if (parseInt(obj.message) > 0) {
                $("#messageBdg").text(obj.message);
                $("#messageBdg").show();
            } else
                $("#messageBdg").hide();
        },
        error: function() {
           // alert();
        }
    });
}

/*
	Function to hide message after clicking on any of the text box
*/
$(".elmnt").click(function() {
    $("#errorMsg").hide(500)
});

/*
	Function to launch messenger applciation
*/
function showMessenger() {
    var sApp = startApp.set({
        "action": "ACTION_MAIN",
        "category": "CATEGORY_DEFAULT",
        "type": "text/css",
        "package": "com.mychance.chat",
        "uri": "file://data/index.html",
        "flags": ["FLAG_ACTIVITY_CLEAR_TOP", "FLAG_ACTIVITY_CLEAR_TASK"],
        "intentstart": "startActivity"
    });
    sApp.start(function() {}, function(error) {});
}

/*
	Function to show registration page
*/
function showRegistrationPage() {
    $("#loginPage").hide();
	$("#firstName").val('');
  	$("#lastName").val('');
    $("#email").val('');
    $("#rgstrUsername").val('');
    $("#rgstrPassword").val('');
	$("#mobile_number").val('');
    $("#registerPage").show();
}

/*
	Function to show login page
*/
function showLoginPage() {
    $("#loginPage").show();
    $("#registerPage").hide();
}

/*
	Function to register user, invoked on register button click
*/
function registerUser() {
    var firstname = $("#firstName").val();
    var lastName = $("#lastName").val();
    var email = $("#email").val();
    var userNameRgstr = $("#rgstrUsername").val();
    var passwordRgstr = $("#rgstrPassword").val();
	var mobile_number = $("#mobile_number").val();
	if(mobile_number.length!=10){
		alert("Invalid mobile number");
		return false;
	}
    var dob = $("#birthDate").val();
	var gender = $("#gender").val();
            $.ajax({
            type: "POST",
            crossDomain: true,
            url: url + "/restCalls/register.php",
            data: {
                "firstname": firstname,
                "lastname": lastName,
                "email": email,
                "password": passwordRgstr,
                "username": userNameRgstr,
                "dob": dob,
                "gender": gender,
				"mobile_number": mobile_number
            },
			beforeSend: function(){
				$('#loader_image').show();
				$('#registerBTN').css('pointer-events', 'none').css('opacity', '0.5');
			},
            success: function(data) {
				var obj = $.parseJSON(data);
                if (obj.success == "0" || obj.success==0) {
                    showLoginPage();
                    $("#errorMsg").text(obj.message);
                    $("#errorMsg").css("display", "block");
                } else {
					 $("#errorMsg").text('');
                    $("#errorMsg").css("display", "none");
					$('#verify-guid').val(obj.guid);
                    showLoginPage();
					sendCode(obj.guid);
					showVerificationPage();
                }
            },
            error: function() {
               // console.log("Registration Failed");
            },
			complete: function(){
				$('#loader_image').hide();
				$('#registerBTN').css('pointer-events', 'auto').css('opacity', '1');
			}
        });
}

function showVerificationPage(){
	$('.card.menu').css('opacity', '0.2');
	$('.verify-parent').show();
}

function verifyCode(){
	var code = $('#verification_text').val();
	var guid = $('#verify-guid').val();
	if(code=="")
		return false;
	$.ajax({
		url: url + "/restCalls/verify.php",
		type: 'POST',
		data: {'code': code, 'guid': guid},
		beforeSend: function(){
			$('#verify-mesg').hide();
			$('#verify_btn').html('<i class="fa fa-circle-o-notch fa-spin fa-3x fa-fw" style="color: #3278b3 !important;margin: 0px;padding: 0px;font-size: 14px !important;"></i>');
		},
		success: function(data){
			if(data==true || data==1){
				showLoginPage();
				$('.card.menu').css('opacity', '1');
				$('.verify-parent').hide();
				$('#verification_text').val("");
				$('#successMsg').html("Verification success. Login to continue.").show();
			}
			else{
				$('#verify-mesg').show();
			}
		},
		error: function(xhr, status, error){
			
		},
		complete: function(){
			$('#verify_btn').html('Verify');
		}
	});
}

/*
	request to send verification code
*/

function sendCode(guid){
	$.ajax({
		url: url+'/restCalls/otp/index.php',
		type: 'POST',
		data: {'guid': guid},
		beforeSend: function(){
			
		},
		success: function(data){
			console.log(data);
		},
		error: function(xhr, status, error){
			
		},
		complete: function(){
			
		}
	});
};

/*
	Show and hide side bar
*/
$("#sidebar-toggle").click(function() {
    if ($("#sidebar-toggle").hasClass("shower")) {
		hideSearch();
		hideMikeIcon()
        $("#sidebar-toggle").removeClass("shower");
        $("#sidebar-toggle").addClass("hider");
        $(".sidebar").addClass("sidebar-open");
        $(".ossn-page-container").addClass("sidebar-open-page-container");
    } else {
		showSearch();
		showMikeIcon();
        $("#sidebar-toggle").removeClass("hider");
        $("#sidebar-toggle").addClass("shower");
        $(".sidebar").removeClass("sidebar-open");
        $(".sidebar").addClass("sidebar-close");
        $(".ossn-page-container").removeClass("sidebar-open-page-container");
    }
});

/*
	Show/hide menu list
*/
$("#menuLister").click(function() {
    $("#menuList").toggle(500);
});

/*
	show/hide group list
*/
$("#groupLister").click(function() {
    $("#groupList").toggle(500);
});

/*
	Function to load new url to iframe
*/
function loadMe(myUrl) {
    var online = onlineOffline();
    if (online) {
        if (myUrl.indexOf('logout.php') > 0) {
			try{
				closeSidebar();
            	logoutMe();
				$('.mike-icon').hide();
			}
			catch(err){
				loadHtml('socialMedia/social_media.html');
			}
        } else {
           // var newUrl = myUrl.replace("userName", storage.getItem("userName"));
            $('#contentIframe').attr('src', myUrl+"?username="+window.localStorage.getItem('userName')+"&password="+window.localStorage.getItem('passWord')+"&session_id="+window.localStorage.getItem('session_id'));
            $("#sidebar-toggle").removeClass("hider");
            $("#sidebar-toggle").addClass("shower");
            $(".sidebar").removeClass("sidebar-open");
            $(".sidebar").addClass("sidebar-close");
            $(".ossn-page-container").removeClass("sidebar-open-page-container");
        }
    }
}

/*
	Function to logout user
*/
function logoutMe() {
    var userName = storage.getItem("userName");
    var newToken = storage.getItem("oldToken");
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/logout.php",
        data: {
            "userName": userName,
            "deviceId": newToken,
			"session_id": window.localStorage.getItem('session_id')
        },
		beforeSend: function(){
			startLoader();
		},
        success: function(data) {
            //console.log(data);
            if (data == "1") {
                //console.log("Logout success");
                $("#contentIframe").css("display", "none");
                $('#contentIframe').attr('src', "");
                $("#loginPage").css("display", "block");
                if (login) {
                    $("#errorMsg").text("Logout Successful.");
                    $("#errorMsg").css("display", "block");
                }
                // remove un,pwd & token from localStorage
                storage.setItem("userName", "");
                storage.setItem("passWord", "");
                storage.setItem("newToken", "");
                storage.setItem("oldToken", "");
				
				//removing groups
				
				$('.groupListli').remove();

                login = false;
            } else {
                //console.log("Logout Failed");
            }
        },
        error: function() {
            //console.log("Logout Failed");
        },
		complete: function(){
			stopLoader();
		}
    });
}



/*
	Function to toggle notification list
*/
$("#notificationIcon").click(function() {
	if($(".friendRequestDropDown").is(':visible'))
		$(".friendRequestDropDown").hide();
	if($(".notificationDropDown").is(':visible')){
		$(".notificationDropDown").hide();
	}
	else{
		$(".notificationDropDown").show();
		getNotifcations();
	}
});



/*
	Function to fetch and list notifications
*/
function getNotifcations() {
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/getNotifications.php",
		data:{
			"userId":id
			},
        success: function(data) {
            $("#notificationArea").html(data);			
        },
        error: function() {
            
        }
    });
}

/*
	Function to hide notification list
*/
function loadThisNotification(myUrl){
	var online = onlineOffline();
    if (online) {
        $('#contentIframe').attr('src', myUrl);
        $("#notificationIcon").removeClass("hider");
        $("#notificationIcon").addClass("shower");
        $(".notificationDropDown").hide();
    }
}

/*
	show post on notification click
*/
/*$(document).on('click', '.ossn-notifications-all a', function(event){
 event.preventDefault();
 //loadThisNotification(this.href);
});*/
/*
	Function to mark all notifications as read
*/
function readAllNotification() {
    $.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/markRead.php",
		data:{
			"userId":id
			},
        success: function(data) {
            //console.log(data);
			$("#notificationIcon").removeClass("hider");
        $("#notificationIcon").addClass("shower");
        $(".notificationDropDown").hide();
        },
        error: function() {
            
        }
    });
}

/*
	Function to toggle friend reqeust list
*/
$("#friendRquestIcon").click(function() {	
	if($(".notificationDropDown").is(":visible")){
		$(".notificationDropDown").hide();
	}
	if($(".friendRequestDropDown").is(":visible"))
		$(".friendRequestDropDown").hide();
	else{
		$(".friendRequestDropDown").show();
		getFriendRequest();
		
	}
});

/*
	Function to get all requests
*/
function getFriendRequest(){
	$.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/getFriendRequests.php",
		data:{
			"userId":id
			},
		beforeSend: function(){
			
		},
        success: function(data) {
            console.log(data);
			$("#requestArea").html(data);
        },
        error: function() {
            
        },
		complete: function(){
			
		}
    });
}
/*
	Prevent default a action, show that url in iframe
*/
$(document).on('click', '.rqsts a', function(event){
 event.preventDefault();
 loadThisRequest(this.href);
});

/*
	Function to load requester profile
*/
function loadThisRequest(myUrl){
	var online = onlineOffline();
    if (online) {
        $('#contentIframe').attr('src', myUrl);
        $("#friendRquestIcon").removeClass("hider");
        $("#friendRquestIcon").addClass("shower");
        $(".friendRequestDropDown").hide();
    }
}

/*
	Function to confirm friend request
*/
function confirmFriend(friendId){
	$.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/addOrRemoveFriend.php",
		data:{
			"userId":id,
			"friendId":friendId,
			"action":"add"
			},
        success: function(data) {
            //console.log(data);
			$("#friendRquestIcon").removeClass("hider");
			$("#friendRquestIcon").addClass("shower");
			$(".friendRequestDropDown").hide();
			sendFriendRequestNotification(friendId, 0);
        },
        error: function() {
            
        }
    });
}

/*
	Function to remove friend request
*/
function removeFriend(friendId){
	$.ajax({
        type: "POST",
        crossDomain: true,
        url: url + "/restCalls/addOrRemoveFriend.php",
		data:{
			"userId":id,
			"friendId":friendId,
			"action":"remove"
			},
        success: function(data) {
            //console.log(data);
			$("#friendRquestIcon").removeClass("hider");
        $("#friendRquestIcon").addClass("shower");
        $(".friendRequestDropDown").hide();
        },
        error: function() {
            
        }
    });
}

function loadHtml(url){
	if($(".friendRequestDropDown").is(':visible'))
		$(".friendRequestDropDown").hide();
	else if($(".notificationDropDown").is(':visible'))
		$(".notificationDropDown").hide();
	showSearch();
	if(url!='socialMedia/social_media.html')
		$('.mike-icon').hide();
	else
		$('.mike-icon').show();
	if(url!='socialMedia/friends.html' && url!='socialMedia/photos.html' && url!='socialMedia/social_media.html')
		backupHome();
	if(url=='socialMedia/friends.html' || url=='socialMedia/photos.html'){
		hideIconsComment();
	}
	 $('#contentIframe').attr('src', url);
            $("#sidebar-toggle").removeClass("hider");
            $("#sidebar-toggle").addClass("shower");
            $(".sidebar").removeClass("sidebar-open");
            $(".sidebar").addClass("sidebar-close");
            $(".ossn-page-container").removeClass("sidebar-open-page-container");
}

function loadFriends(){
	viewFriends( storage.getItem("userid"));
	 //$('#contentIframe').attr('src', url);
            $("#sidebar-toggle").removeClass("hider");
            $("#sidebar-toggle").addClass("shower");
            $(".sidebar").removeClass("sidebar-open");
            $(".sidebar").addClass("sidebar-close");
            $(".ossn-page-container").removeClass("sidebar-open-page-container");
}

function closeSidebar(){
	$("#sidebar-toggle").removeClass("hider");
	$("#sidebar-toggle").addClass("shower");
	$(".sidebar").removeClass("sidebar-open");
	$(".sidebar").addClass("sidebar-close");
	$(".ossn-page-container").removeClass("sidebar-open-page-container");
}

/*
	FUNCTION TO ADD A GROUP
*/

$(document).on('click', '#ossn-group-add', function() {
	//var $this = $('#contentIframe').contents().find('.ossn-message-box').html('sfds').fadeIn();
	event.preventDefault();
	if($('#contentIframe').contents().find('.ossn-layout-newsfeed').length==0){
		window.localStorage.setItem('funct', 'window.parent.addGroup()');
		loadHtml('socialMedia/social_media.html');
		return false;
	}
	addGroup();
});

function addGroup(){
	$.ajax({
		url: getBaseUrl() + "restCalls/application/group/add_group_form.php",
		type: "POST",
		data: {'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			closeSidebar();
			$('#contentIframe').contents().find('.ossn-halt').addClass('ossn-light');
			$('#contentIframe').contents().find('.ossn-halt').attr('style', 'height:' + $(document).height() + 'px;');
			$('#contentIframe').contents().find('.ossn-halt').show();
			$('#contentIframe').contents().find('.ossn-message-box').html('<div class="ossn-loading ossn-box-loading"></div>');
			$('#contentIframe').contents().find('.ossn-message-box').fadeIn('slow');
		},
		success: function(data){
			$('#contentIframe').contents().find('.ossn-message-box').html(data).fadeIn();
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			
		}
	});
}

/*
	FUNCTION TO VIEW GROUP
*/
function viewGroup(guid){
	window.localStorage.setItem("offset", 1);
	window.localStorage.setItem('loaded_item', 'viewgroup');
	document.getElementById('contentIframe').contentWindow.resetGlobalvars();
	hideMikeIcon();
	if($('#contentIframe').contents().find('.ossn-layout-newsfeed').length==0){
		window.localStorage.setItem('funct', 'window.parent.viewGroup('+guid+')');
		loadHtml('socialMedia/social_media.html');
		return false;
	}
	$.ajax({
		url: url + "/restCalls/application/group/actions/view.php",
		type: "POST",
		data: {'guid':guid,'offset':1,'session_id': window.localStorage.getItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			closeSidebar();
			$('#contentIframe').contents().find('.loader-wrapper').show();
			$('#contentIframe').contents().find('.no-loding').css('pointer-events', 'none');
			$('#contentIframe').contents().find('.no-loding').css('opacity', '0.4');
		},
		success: function(data){
			//console.log(data);
			if(parseInt(window.localStorage.getItem('offset'))==1){
				$('#contentIframe').contents().find('.ossn-layout-newsfeed').html(data);
			}
			else{
				$('#contentIframe').contents().find('.ossn-layout-newsfeed div.ossn-wall-item:last').after(data);
			}
			if($('#contentIframe').contents().find('#loaded-group-id').length)
				$('#contentIframe').contents().find('#loaded-group-id').val(guid);
			else
				$('#contentIframe').contents().find('.ossn-inner-page').append('<input type="hidden" id="loaded-group-id" value="'+guid+'">');
			$('#contentIframe').contents().find('.pagination.ossn-pagination').addClass('groupTimeLine');
			
		},
		error: function(xhr, status, error){
			//console.log(xhr.responseText);
		},
		complete: function(){
			$('#contentIframe').contents().find('.loader-wrapper').hide();
			$('#contentIframe').contents().find('.no-loding').css('pointer-events', 'auto');
			$('#contentIframe').contents().find('.no-loding').css('opacity', '1');
		}
	});
}

/*
	FUNCTION TO HANDLE SEARCH
*/

/**/
$(document).on("keyup", '#searchBox', function(event){
	if(event.which == 13) {
		return false;
	}
});

$(document).on('click', '.search-btn', function(event){
	console.log(1);
	console.log($("#searchBox").val());
	if($("#searchBox").val()=='')
		return false;
	console.log(2);
	searchMe($("#searchBox").val());
});

function searchMe(val){
	console.log(3);
	var query = val;
	$.ajax({
		url: url + "/restCalls/application/search.php",
		type: "POST",
		data: {'q':query, 'offset': 1, 'session_id': window.localStorage.getItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			closeSidebar();
			$("#searchBox").val('');
			startLoader();
		},
		success: function(data){
			console.log(data);
			$('#contentIframe').contents().find('.ossn-layout-newsfeed').html(data);
			var html = '<div class="ossn-menu-search"><div class="title">RESULT TYPE</div><li class="ossn-menu-search-users"><a href="search?type=users&amp;q=sf"><div class="text">People</div></a></li><li class="ossn-menu-search-groups"><a href="search?type=groups&amp;q=sf"><div class="text">Teams</div></a></li></div>';
			$('#contentIframe').contents().find('.coloum-left.ossn-page-contents').html(html);
			$('#contentIframe').contents().find('.pagination.ossn-pagination').addClass('searchResults');
			if($('#contentIframe').contents().find('#searched-query').length)
				$('#contentIframe').contents().find('#searched-query').val(val);
			else
				$('#contentIframe').contents().find('.no-loding').append('<input type="hidden" id="searched-query" value="'+val+'"><input type="hidden" id="searched-type" value="users">');
			
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			stopLoader();
		}
	});

}
/*
	LOAD TIMELINE FROM SIDEBAR / FRIEND REQUEST TAB
*/
function LoadTimeline(username){
	closeSidebar();
	document.getElementById('contentIframe').contentWindow.resetGlobalvars();
	if(checkIfloaded(username)){
		if(username=='')
			document.getElementById('contentIframe').contentWindow.LoadUserTimeLine(window.localStorage.getItem('userName'), 1);
		else{
			$("#friendRquestIcon").click();
			document.getElementById('contentIframe').contentWindow.LoadUserTimeLine(username, 1);
		}
	}
}

function checkIfloaded(username){
	if($('#contentIframe').contents().find('.ossn-layout-newsfeed').length==0){
		if(username=='')
			username=window.localStorage.getItem('userName');
		window.localStorage.setItem('funct', 'LoadUserTimeLine("'+username+'",1)');
		loadHtml('socialMedia/social_media.html');
		return false;
	}
	return true;
}

/*
	LOAD EDIT PROFILE PAGE FROM SIDEBAR
*/

function LoadUserEditPage(){
	closeSidebar();
	if($('#contentIframe').contents().find('.ossn-layout-newsfeed').length==0){
		window.localStorage.setItem('funct', 'LoadEditUserPage("'+window.localStorage.getItem('userid')+'")');
		loadHtml('socialMedia/social_media.html');
		return false;
	}
	document.getElementById('contentIframe').contentWindow.LoadEditUserPage(window.localStorage.getItem('userid'));
}

/*
	AJAX LOADING
*/
function startLoader(){
	$('#contentIframe').contents().find('.loader-wrapper').show();
	$('#contentIframe').contents().find('.no-loding').css('pointer-events', 'none');
	$('#contentIframe').contents().find('.no-loding').css('opacity', '0.4');

}
function stopLoader(){
	$('#contentIframe').contents().find('.loader-wrapper').hide();
	$('#contentIframe').contents().find('.no-loding').css('pointer-events', 'auto');
	$('#contentIframe').contents().find('.no-loding').css('opacity', '1');
}


/*
	NOTIFICATION VIEWS
*/


$(document).on('click', '.ossn-notifications-all li', function(event){
	event.preventDefault();
	$('#notificationIcon').click();
	hideIconsComment(0);
	var $this = $(this);
	var type = $this.attr('data-type');
	//Group join request
	if(type=='group-join'){
		var guid = $this.attr('data-group');
		document.getElementById('contentIframe').contentWindow.viewSubpages('requests', guid);
	}
	//annotation
	else if(type==="annotation-like"){
		var category = $this.attr('data-category');
		//post like
		if(category=='post'){
			var guid = $this.attr('data-guid');
			viewLikedPost(guid);
		}
	}
	else if(type=='profile-pic'){
		var guid = $this.attr('data-guid');
		document.getElementById('contentIframe').contentWindow.loadPhoto(guid, 'profile', 'out');
	}
	else if(type=='wall-post'){
		var guid = $this.attr('data-guid');
		viewLikedPost(guid);
	}
	else if(type=='group-post'){
		var guid = $this.attr('data-guid');
		viewLikedPost(guid);
	}
	else if(type=='group-invite'){
		var guid = $this.attr('data-group');
		viewGroup(guid);
	}
	else{
		//do nothing
	}
});

function viewLikedPost(guid){
	
	$.ajax({
		url: getBaseUrl() + "restCalls/application/notification/post.php",
		type: "POST",
		data: {'guid':guid, 'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$('#notificationIcon').click();
			startLoader();
		},
		success: function(data){
			$('#contentIframe').contents().find('.ossn-layout-newsfeed').html(data);
		},
		error: function(xhr, status, error){
			alert(xhr.responseText);
		},
		complete: function(){
			stopLoader();
		}
	});
	
}

function LoadAllNotifications(){
	$.ajax({
		url: getBaseUrl() + "restCalls/application/notification/all.php",
		type: "POST",
		data: {'session_id': getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			$('#notificationIcon').click();
			startLoader();
		},
		success: function(data){
			$('#contentIframe').contents().find('.ossn-layout-newsfeed').html(data);
		},
		error: function(xhr, status, error){
			//alert(xhr.responseText);
		},
		complete: function(){
			stopLoader();
		}
	});
}

/*
	DISPLAY GROUP NAME ON GROUP ADD SUCCESS
*/
function groupAdd(obj){
	var html = '<li class="groupListli" id="groupLi'+obj.guid+'"  onclick="viewGroup('+obj.guid+')"><a href="#">'+obj.name+'</a></li>';
	$('#groupList').prepend(html);
}

/*
	REMOVE GROUP NAME ON GROUP DELETE SUCCESS
*/

function groupDelete(guid){
	$('#groupLi'+guid).hide();
}

/*
	LOAD USER TIMELINE
*/
$(document).on('click', '.userlink', function(event){
	event.preventDefault();
	var string = $(this).attr('href');
	var username = string.substring(string.lastIndexOf("/") + 1);
	//alert(username);
	LoadUserTimeLine(username, 1);
	
});

function LoadUserTimeLine(username, offset){
	$.ajax({
		url: getBaseUrl()+"restCalls/application/user/profile.php",
		type: "POST",
		data: {"username": username, 'offset':offset, "session_id": getLocalStorageItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			//alert(data);
			$('#contentIframe').contents().find('.ossn-layout-newsfeed').html(data);
			document.getElementById('contentIframe').contentWindow.addOffsetClass('userTimeLine');
		},
		error: function(xhr, status, error){
			alert(xhr.responseText);
		},
		complete: function(){
		}
	});
}

function backupHome(){
	closeSidebar();
	var html = $('body').html();
	modifyStyle();
	window.localStorage.setItem("homeHtml", html);
}
function restoreHome(){
	/*var html = window.localStorage.getItem("homeHtml");
	$('body').html(html);*/
	$('.topbar').show();
	//$('#contentIframe').removeClass('modify_style');
	loadHtml('socialMedia/social_media.html');
}

function modifyStyle(){
	$('.topbar').hide();
	//$('#contentIframe').css('top', '0%').css('height', '100%');
	//$('#contentIframe').addClass('modify_style');
}

function restoreStyle(){
	$('.topbar').show();
	//$('#contentIframe').css('top', '6%').css('height', '94%');
}

function setImageandName(img, name){
	window.localStorage.setItem('proimg', img);
	window.localStorage.setItem('proname', name);
}

$(document).on('click', '.mike-icon', function(event){
	closeSidebar();
	$('.mike-icon img').hide();
	document.getElementById('contentIframe').contentWindow.showPostAddPage();
});

function showFullscreen(src, type){
	$('#fullScreenPage').html("");
	hideMikeIcon();
	var html = "<i class='fa fa-arrow-left' onclick='hideFullScreen()' style='position: fixed;z-index:99;top: 5px;left: 5px;'></i>";
	if(type=='img'){
 		html= html + "<img class='fullscreen-content' src='"+src+"' >";
	}
	else if(type=='video'){
		//src = src.slice(0,-3);
		html = html + "<video  poster='img/post.jpg' class='fullscreen-content' id='fullscreenvideo'><source type='video/mp4' src='"+src+"'></video>";
	}
	else if(type=='invite-friends'){
		$('#fullScreenPage').css('background', 'white');
		html = html + getFriends(window.localStorage.getItem("group_id"));
	}
	/*$('.ossn-page-container.myTitleBar.hidediv').hide();
	$('.h_iframe.hidediv').hide();*/
	if(type!='invite-friends')
		$('#fullScreenPage').html(html).show();
	if(type=='video'){
		//$('#fullscreenvideo').attr('controls', 'controls');
		//$('#fullscreenvideo').get(0).currentTime=0;
		
		$('#fullscreenvideo').get(0).play();
		Fullscreen.on();
	}
}

$(document).on('click', '#fullscreenvideo', function(e){
	var attr = $(this).attr('controls');
	if (typeof attr !== typeof undefined && attr !== false) {
	  
	}
	else{
		$(this).attr('controls', 'controls');
	}
});
function getFriends(group_id){
	var html = "";
	$.ajax({
		url: getBaseUrl()+"restCalls/application/group/getFriends.php",
		type: "POST",
		data: {"session_id": window.localStorage.getItem('session_id')},
		async: true,
		cache: false,
		crossDomain: true,
		
		beforeSend: function(){
			
		},
		success: function(data){
			html = html + preapareInviteHtml(data);
			$('#fullScreenPage').html(html).show();
		},
		error: function(xhr, status, error){
			alert(xhr.responseText);
		},
		complete: function(){
		}
	});
	return html;
}

function preapareInviteHtml(data){
	var obj = $.parseJSON(data);
	var html = "<div style='background: #3278b3;color: white;text-align: center;padding: 15px;font-size: 18px;position: fixed;width: 100%;'>Select Friends to invite</div><div style='padding:15px;margin-top: 40px;'><table id='invite-friends-list' width='100%' style='border-collapse: separate;border-spacing: 10px;'><tr><td colspan='2'><input type='text' id='search-invite-friends' class='form-control' placeholder='Search'></td></tr>";
	for(var i=0;obj[i]!=null;i++){
		html = html + "<tr class='hide-tr'><td><input class='invite-friends-checkbox' type='checkbox' style='width: 21px;height: 18px;' value='"+obj[i].guid+"'></td><td>"+obj[i].first_name+" "+obj[i].last_name+"</td></tr>"
	}
	html= html+"</table></div><div style='position: fixed;bottom:0%;width:100%;'><table width='100%'><tr><td width='50%' style='height: 40px; background: #3278b3; text-align: center; color: white; font-size: 15px; border-right: 1px solid white;' onclick='showHome()'>Cancel</td><td width='50%' onclick='inviteFriendsToTeam()' style='height: 40px; background: #3278b3; text-align: center; color: white; font-size: 15px;'>Invite</td></tr></table></div>";
	return html;
}

function showMikeIcon(){
	$('.mike-icon img').show(300);
	showSearch();
}
function hideMikeIcon(){
	$('.mike-icon img').hide(300);
	//hideSearch();
}
function hideFullScreen(){
	Fullscreen.off();
	$('#fullScreenPage').css('background', 'black');
	$('#fullScreenPage').html("").hide();
	if($('#contentIframe').contents().find('#identifier').length){
		showMikeIcon();
	}
	$('.ossn-page-container.myTitleBar.hidediv').show();
	$('.h_iframe.hidediv').show();
}
function commentFullView(content){
	/*$('.h_iframe').hide();
	$('#comment-page').html(content).show();;
	$('#comment-page').find('.comments-list').show();
	$("html, #comment-page").animate({ scrollTop: $(document).height() }, 100);*/
	//$('.comment-page').find('#comment-box-'+guid).focus();
}
function hideIconsComment(isComment){
	$('#sidebar-toggle').hide();
	if(isComment)
		$('#commnt-backbtn').show().attr('onclick', 'showIconsComment(1)');
	else
		$('#commnt-backbtn').show().attr('onclick', 'showIconsComment(0)');
	$('.topbar-menu-right').hide();	
}
function showIconsComment(isComment){
	if(isComment==1){
		document.getElementById('contentIframe').contentWindow.closeCommentViewPage();
		$('#sidebar-toggle').show();
		$('#commnt-backbtn').hide();
		$('.topbar-menu-right').show();
	}	
	else{
		$('#sidebar-toggle').show();
		$('#commnt-backbtn').hide();
		$('.topbar-menu-right').show();
		loadHtml('socialMedia/social_media.html');
	}
}

function inviteFriends(group_id){
	window.localStorage.setItem("group_id", group_id);
	showFullscreen('', 'invite-friends');
}

function inviteFriendsToTeam(){
	var friends = Array();
	$('.invite-friends-checkbox:checked').each(function(e) {
        friends.push($(this).val());
    });
	var group_id = window.localStorage.getItem('group_id');
	if(friends.length>0 && group_id!=null){
		$.ajax({
			url: getBaseUrl()+"restCalls/application/group/inviteFriends.php",
			type: "POST",
			data: {"session_id": window.localStorage.getItem('session_id'), 'friends': JSON.stringify(friends), 'group_id':group_id},
			async: true,
			cache: false,
			crossDomain: true,
			
			beforeSend: function(){
				
			},
			success: function(data){
				sendGroupInvitationNotification(friends);
				navigator.notification.alert(
					'Invitation sent!',  
					showHome,        
					'Success',           
					'OK'                 
				);
			},
			error: function(xhr, status, error){
				alert(xhr.responseText);
			},
			complete: function(){
			}
		});
	}
	else{
		return false;
	}
}

jQuery.expr[':'].Contains = function(a,i,m){
    return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase())>=0;
};

function showHome(){
	hideFullScreen();
}

$(document).on('keyup', '#search-invite-friends', function(e){
	$('.hide-tr').hide();
	$('tr:Contains('+$(this).val()+')').show();
});

function playVideo(videoUrl){
	try{
		var options = {
				successCallback: function() {
				},
				errorCallback: function(errMsg) {
				  alert("This video cannot be played");
				},
				orientation: 'landscape',
				shouldSplitVertically: false,
				shouldAutoClose: true
				};
		window.plugins.streamingMedia.playVideo(videoUrl, options);
  }catch(err){
  	alert("This video cannot be played");
  }
  // Play a video with callbacks
  
}

//toggle searchbox on/off
function showSearchBox(){
	$('.hide_td_t').hide();
	$('.search_td').show();
	$('#searchBox').focus();
}

function hideSearchBox(){
	$('.hide_td_t').show();
	$('.search_td').hide();
}

function showSearch(){
	$('.search_container').show();
}

function hideSearch(){
	$('.search_container').hide();
}

function openHelp(){
	$('#help_menu').css('font-size', '18px');
	window.open('http://mychance.in/help/', '_system');
	$('#help_menu').css('font-size', '15px');
}

function showBackButton(){
	
}