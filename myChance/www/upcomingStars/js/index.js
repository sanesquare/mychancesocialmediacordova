var deviceId;
var login;
var url = "http://m.mychance.in";
//var url = "http://demo.snogol.net/mychance.in/m.mychance";
//var url = "http://192.168.1.6/m.mychance";
var storage = window.localStorage;
var fresh = false;



var app = {
    initialize: function() {
        this.bindEvents();
    },
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);

    },
    onDeviceReady: function() {
        document.addEventListener("pause", checkMessenger, false);
        document.addEventListener("resume", checkMessenger, false);
        checkMessenger();
        fresh = true;
        var push = PushNotification.init({
            "android": {
                "senderID": "668368470938"
            },
            "ios": {
                "alert": "true",
                "badge": "true",
                "sound": "true"
            },
            "windows": {}
        });
        push.on('registration', function(data) {
            deviceId = data.registrationId;
            var userName = storage.getItem("userName");
            var passWord = storage.getItem("passWord");
            var oldToken = storage.getItem("oldToken");
            try {
                if (userName.length > 0 && passWord.length > 0)
                    loginServer(userName, passWord, oldToken, deviceId);
                else {
                    $("#loginPage").css("display", "block");
                }
            } catch (e) {
                $("#loginPage").css("display", "block");
            }
        });

        push.on('notification', function(data) {
            // data.message,
            // data.title,
            // data.count,
            // data.sound,
            // data.image,
            // data.additionalData
        });

        push.on('error', function(e) {
            // e.message
        });
        if (navigator.connection.type == 'none') {
            $('#contentIframe').attr('src', "error.html");
        } else {
            //$('#contentIframe').attr('src', "http://demo.snogol.net/mychance.in");
        }
    },
    receivedEvent: function(id) {}
};

